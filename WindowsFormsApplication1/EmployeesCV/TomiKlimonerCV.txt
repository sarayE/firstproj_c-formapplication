TOMI KLIMONER
71 Fifth Ave.
Sometown, NY 11597
555.555.5555
Tomi@walla.co.il

SENIOR PROGRAMMER

    * Results-driven lead programmer offering extensive experience within both Fortune 500 and startup environments. SCJP and SCJD certified.
    * Able to incorporate user needs into cost-effective, secure and user-friendly solutions known for scalability and durability.
    * Innovator of next-generation solutions, systems and applications fueling major improvements to the bottom line.
    * Proven leader and project manager; drive system architecture decisions and lead projects from concept through the release process.

Technology Summary :

Languages/Programming: Java, J2EE, C, C++, Perl, PHP, VB.Net, SQL Server, ASP.Net, HTML, XML, SAP, ActiveX

Systems: AIX, Linux, Unix (Solaris/HP-UX), Windows XP/NT v4.0

Networking: TCP/IP, SFTP, FTP

Databases: DB2, Oracle 8/7, Informix, MS Access