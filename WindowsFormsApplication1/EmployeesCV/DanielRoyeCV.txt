DANIEL RTYE
157 Park Road Luton L1 6RR
Danir@bezeqint.net


Personal profile:
A highly motivated recent Business Studies graduate
looking to kick start his career as a financial advisor
and build his business building skills with a reputable
finance company.

Possessing enthusiasm, drive and a positive attitude
required to be successful in a sales and marketing
environment. Presently looking for a graduate or
trainee financial advisor position with a rewarding and
forward looking company.
Academic qualifications
BA (Hons) Business Studies 2:1
Certificate in Financial Planning (CFP)
A Levels:
Maths (B)
English(A)
Geography (C)
Work experience
MORTGAGE ADVISOR (part time)
First Base Mortgages June 2010 - Present
Giving advice on mortgages and related products like
investments, life insurance, critical illness cover and
pensions. Working in the early evening, contacting
clients via telephone and occasionally meeting them
face to face.

Duties:

Helping clients to find financial products that suit their
needs.
Doing general administrative office work.
Having to explain financial matters clearly, with the
minimum of jargon.
Conducting in-depth reviews of clients financial
circumstances.
Researching and finding the best financial products.
Contacting other professionals like estate agents,
solicitors and valuers.
Making sure all paperwork is completed correctly to
comply with the regulations of the Financial Services
Authority (FSA).
Setting up meetings with clients.