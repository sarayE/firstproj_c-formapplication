YAKOV LEVY
157 Park Road Luton L1 6RR
Tel: 0161 555 3333 Mobile: 07766 111 2222 
YakovLevy@zahav.co.il

Voluntary experience

Volunteer Advisor (part - time)
Citizens Advice Bureau January 2010 to June 2010
Spending Saturdays by providing people with free
legal advice to help resolve any problems or issues
they have.
Duties:
Making telephone calls and enquiries on behalf of
clients.
Occasionally going along with them to court hearings
or at tribunals.

Writing letters on behalf of people.
Explaining to people how we can help them.
Administrative duties like filing, updating records.
Finding out about a clients problems.
Answering emails and phone enquiries.
Getting involved in media campaigns.
Helping with fundraising activities.

Personal skills

* Success and results driven.
* Excellent numeracy and IT skills.
* Research and analyse financial information.
* Proven sales and negotiation skills.
* Punctual and well presented.
* Confident outlook.
* Good listener.
* Able to work individually or in a team.
* Open minded and non-judgemental.
* Attention to detail.
* Adaptable to new situations.
* Enjoy a competitive environment.
* High energy levels.
* Able to work under pressure.
* Problem solving.