﻿namespace WindowsFormsApplication1
{
    partial class FormHR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControlGrds = new System.Windows.Forms.TabControl();
            this.tabAllEmp = new System.Windows.Forms.TabPage();
            this.grdEmp = new System.Windows.Forms.DataGridView();
            this.tblID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblSkills = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblSalary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblImage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblGander = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabAllSkills = new System.Windows.Forms.TabPage();
            this.grdSkl = new System.Windows.Forms.DataGridView();
            this.tblBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblSkillsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblSkillDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblSkillLinq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpSearchEmp = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radDate = new System.Windows.Forms.RadioButton();
            this.radSkill = new System.Windows.Forms.RadioButton();
            this.radSalary = new System.Windows.Forms.RadioButton();
            this.radEmail = new System.Windows.Forms.RadioButton();
            this.radID = new System.Windows.Forms.RadioButton();
            this.radLastName = new System.Windows.Forms.RadioButton();
            this.radFirstName = new System.Windows.Forms.RadioButton();
            this.txtSalaryTo = new System.Windows.Forms.TextBox();
            this.txtSalaryFrom = new System.Windows.Forms.TextBox();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.btnSearchClear = new System.Windows.Forms.Button();
            this.timeSearchDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblSkills = new System.Windows.Forms.Label();
            this.cmbSkills = new System.Windows.Forms.ComboBox();
            this.btnSearchGo = new System.Windows.Forms.Button();
            this.lblSearch = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addPhotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePhotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBoxEmp = new System.Windows.Forms.PictureBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.stpAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.stpNewEmp = new System.Windows.Forms.ToolStripMenuItem();
            this.stpNewSkill = new System.Windows.Forms.ToolStripMenuItem();
            this.stpEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.stpDelet = new System.Windows.Forms.ToolStripMenuItem();
            this.stpDeletEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.stpDeletAllEmp = new System.Windows.Forms.ToolStripMenuItem();
            this.stpDeletAllSkills = new System.Windows.Forms.ToolStripMenuItem();
            this.stpHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.stpExit = new System.Windows.Forms.ToolStripMenuItem();
            this.stpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTotalSkills = new System.Windows.Forms.Label();
            this.lblTotalSalaries = new System.Windows.Forms.Label();
            this.lblTotalEmp = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTxtBox_Skill = new System.Windows.Forms.RichTextBox();
            this.richTxtBox_Emp = new System.Windows.Forms.RichTextBox();
            this.tabControlEmpInfo = new System.Windows.Forms.TabControl();
            this.tabEmpInfo = new System.Windows.Forms.TabPage();
            this.tabCV = new System.Windows.Forms.TabPage();
            this.tabControlGrds.SuspendLayout();
            this.tabAllEmp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEmp)).BeginInit();
            this.tabAllSkills.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSkl)).BeginInit();
            this.grpSearchEmp.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmp)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControlEmpInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlGrds
            // 
            this.tabControlGrds.Controls.Add(this.tabAllEmp);
            this.tabControlGrds.Controls.Add(this.tabAllSkills);
            this.tabControlGrds.Location = new System.Drawing.Point(7, 36);
            this.tabControlGrds.Name = "tabControlGrds";
            this.tabControlGrds.SelectedIndex = 0;
            this.tabControlGrds.Size = new System.Drawing.Size(660, 427);
            this.tabControlGrds.TabIndex = 1;
            this.tabControlGrds.SelectedIndexChanged += new System.EventHandler(this.tabControlGrds_SelectedIndexChanged);
            // 
            // tabAllEmp
            // 
            this.tabAllEmp.Controls.Add(this.grdEmp);
            this.tabAllEmp.Location = new System.Drawing.Point(4, 25);
            this.tabAllEmp.Name = "tabAllEmp";
            this.tabAllEmp.Padding = new System.Windows.Forms.Padding(3);
            this.tabAllEmp.Size = new System.Drawing.Size(652, 398);
            this.tabAllEmp.TabIndex = 0;
            this.tabAllEmp.Text = "All The Employees";
            this.tabAllEmp.UseVisualStyleBackColor = true;
            // 
            // grdEmp
            // 
            this.grdEmp.AllowUserToAddRows = false;
            this.grdEmp.AllowUserToDeleteRows = false;
            this.grdEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEmp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tblID,
            this.tblFirstName,
            this.tblLastName,
            this.tblSkills,
            this.tblSalary,
            this.tblEmail,
            this.tblStartDate,
            this.tblEndDate,
            this.tblImage,
            this.tblGander});
            this.grdEmp.Location = new System.Drawing.Point(3, 4);
            this.grdEmp.Name = "grdEmp";
            this.grdEmp.ReadOnly = true;
            this.grdEmp.RowTemplate.Height = 24;
            this.grdEmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdEmp.Size = new System.Drawing.Size(646, 391);
            this.grdEmp.TabIndex = 0;
            this.grdEmp.SelectionChanged += new System.EventHandler(this.grdEmp_SelectionChanged);
            // 
            // tblID
            // 
            this.tblID.HeaderText = "ID";
            this.tblID.Name = "tblID";
            this.tblID.ReadOnly = true;
            this.tblID.Width = 40;
            // 
            // tblFirstName
            // 
            this.tblFirstName.HeaderText = "First Name";
            this.tblFirstName.Name = "tblFirstName";
            this.tblFirstName.ReadOnly = true;
            // 
            // tblLastName
            // 
            this.tblLastName.HeaderText = "Last Name";
            this.tblLastName.Name = "tblLastName";
            this.tblLastName.ReadOnly = true;
            // 
            // tblSkills
            // 
            this.tblSkills.HeaderText = "Skills";
            this.tblSkills.Name = "tblSkills";
            this.tblSkills.ReadOnly = true;
            this.tblSkills.Width = 140;
            // 
            // tblSalary
            // 
            this.tblSalary.HeaderText = "Salary";
            this.tblSalary.Name = "tblSalary";
            this.tblSalary.ReadOnly = true;
            this.tblSalary.Width = 70;
            // 
            // tblEmail
            // 
            this.tblEmail.HeaderText = "Email";
            this.tblEmail.Name = "tblEmail";
            this.tblEmail.ReadOnly = true;
            // 
            // tblStartDate
            // 
            this.tblStartDate.HeaderText = "Start Date";
            this.tblStartDate.Name = "tblStartDate";
            this.tblStartDate.ReadOnly = true;
            // 
            // tblEndDate
            // 
            this.tblEndDate.HeaderText = "End Date";
            this.tblEndDate.Name = "tblEndDate";
            this.tblEndDate.ReadOnly = true;
            // 
            // tblImage
            // 
            this.tblImage.HeaderText = "Image";
            this.tblImage.Name = "tblImage";
            this.tblImage.ReadOnly = true;
            this.tblImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tblImage.Visible = false;
            // 
            // tblGander
            // 
            this.tblGander.HeaderText = "Gander";
            this.tblGander.Name = "tblGander";
            this.tblGander.ReadOnly = true;
            this.tblGander.Visible = false;
            // 
            // tabAllSkills
            // 
            this.tabAllSkills.Controls.Add(this.grdSkl);
            this.tabAllSkills.Location = new System.Drawing.Point(4, 25);
            this.tabAllSkills.Name = "tabAllSkills";
            this.tabAllSkills.Padding = new System.Windows.Forms.Padding(3);
            this.tabAllSkills.Size = new System.Drawing.Size(652, 398);
            this.tabAllSkills.TabIndex = 3;
            this.tabAllSkills.Text = "All The Skills";
            this.tabAllSkills.UseVisualStyleBackColor = true;
            // 
            // grdSkl
            // 
            this.grdSkl.AllowUserToAddRows = false;
            this.grdSkl.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdSkl.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdSkl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSkl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tblBarcode,
            this.tblSkillsName,
            this.tblSkillDescription,
            this.tblSkillLinq});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdSkl.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdSkl.Location = new System.Drawing.Point(0, 0);
            this.grdSkl.Name = "grdSkl";
            this.grdSkl.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdSkl.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdSkl.RowTemplate.Height = 24;
            this.grdSkl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdSkl.Size = new System.Drawing.Size(649, 395);
            this.grdSkl.TabIndex = 0;
            this.grdSkl.SelectionChanged += new System.EventHandler(this.grdSkl_SelectionChanged);
            // 
            // tblBarcode
            // 
            this.tblBarcode.HeaderText = "Barcode";
            this.tblBarcode.Name = "tblBarcode";
            this.tblBarcode.ReadOnly = true;
            this.tblBarcode.Width = 70;
            // 
            // tblSkillsName
            // 
            this.tblSkillsName.HeaderText = "Name";
            this.tblSkillsName.Name = "tblSkillsName";
            this.tblSkillsName.ReadOnly = true;
            // 
            // tblSkillDescription
            // 
            this.tblSkillDescription.HeaderText = "Description";
            this.tblSkillDescription.Name = "tblSkillDescription";
            this.tblSkillDescription.ReadOnly = true;
            this.tblSkillDescription.Width = 400;
            // 
            // tblSkillLinq
            // 
            this.tblSkillLinq.HeaderText = "Linq";
            this.tblSkillLinq.Name = "tblSkillLinq";
            this.tblSkillLinq.ReadOnly = true;
            // 
            // grpSearchEmp
            // 
            this.grpSearchEmp.Controls.Add(this.groupBox2);
            this.grpSearchEmp.Controls.Add(this.txtSalaryTo);
            this.grpSearchEmp.Controls.Add(this.txtSalaryFrom);
            this.grpSearchEmp.Controls.Add(this.lblTo);
            this.grpSearchEmp.Controls.Add(this.lblFrom);
            this.grpSearchEmp.Controls.Add(this.btnSearchClear);
            this.grpSearchEmp.Controls.Add(this.timeSearchDate);
            this.grpSearchEmp.Controls.Add(this.lblDate);
            this.grpSearchEmp.Controls.Add(this.lblSkills);
            this.grpSearchEmp.Controls.Add(this.cmbSkills);
            this.grpSearchEmp.Controls.Add(this.btnSearchGo);
            this.grpSearchEmp.Controls.Add(this.lblSearch);
            this.grpSearchEmp.Controls.Add(this.txtSearch);
            this.grpSearchEmp.Location = new System.Drawing.Point(11, 524);
            this.grpSearchEmp.Name = "grpSearchEmp";
            this.grpSearchEmp.Size = new System.Drawing.Size(649, 212);
            this.grpSearchEmp.TabIndex = 2;
            this.grpSearchEmp.TabStop = false;
            this.grpSearchEmp.Text = "Search Employee By:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radDate);
            this.groupBox2.Controls.Add(this.radSkill);
            this.groupBox2.Controls.Add(this.radSalary);
            this.groupBox2.Controls.Add(this.radEmail);
            this.groupBox2.Controls.Add(this.radID);
            this.groupBox2.Controls.Add(this.radLastName);
            this.groupBox2.Controls.Add(this.radFirstName);
            this.groupBox2.Location = new System.Drawing.Point(3, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 37);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            // 
            // radDate
            // 
            this.radDate.AutoSize = true;
            this.radDate.Location = new System.Drawing.Point(464, 10);
            this.radDate.Name = "radDate";
            this.radDate.Size = new System.Drawing.Size(59, 21);
            this.radDate.TabIndex = 12;
            this.radDate.Text = "Date";
            this.radDate.UseVisualStyleBackColor = true;
            this.radDate.CheckedChanged += new System.EventHandler(this.radDate_CheckedChanged);
            // 
            // radSkill
            // 
            this.radSkill.AutoSize = true;
            this.radSkill.Location = new System.Drawing.Point(404, 10);
            this.radSkill.Name = "radSkill";
            this.radSkill.Size = new System.Drawing.Size(54, 21);
            this.radSkill.TabIndex = 5;
            this.radSkill.Text = "Skill";
            this.radSkill.UseVisualStyleBackColor = true;
            this.radSkill.CheckedChanged += new System.EventHandler(this.radSkill_CheckedChanged);
            // 
            // radSalary
            // 
            this.radSalary.AutoSize = true;
            this.radSalary.Location = new System.Drawing.Point(329, 11);
            this.radSalary.Name = "radSalary";
            this.radSalary.Size = new System.Drawing.Size(69, 21);
            this.radSalary.TabIndex = 26;
            this.radSalary.Text = "Salary";
            this.radSalary.UseVisualStyleBackColor = true;
            this.radSalary.CheckedChanged += new System.EventHandler(this.radSalary_CheckedChanged);
            // 
            // radEmail
            // 
            this.radEmail.AutoSize = true;
            this.radEmail.Location = new System.Drawing.Point(260, 10);
            this.radEmail.Name = "radEmail";
            this.radEmail.Size = new System.Drawing.Size(63, 21);
            this.radEmail.TabIndex = 3;
            this.radEmail.Text = "Email";
            this.radEmail.UseVisualStyleBackColor = true;
            this.radEmail.CheckedChanged += new System.EventHandler(this.radEmail_CheckedChanged);
            // 
            // radID
            // 
            this.radID.AutoSize = true;
            this.radID.Location = new System.Drawing.Point(212, 10);
            this.radID.Name = "radID";
            this.radID.Size = new System.Drawing.Size(42, 21);
            this.radID.TabIndex = 2;
            this.radID.Text = "ID";
            this.radID.UseVisualStyleBackColor = true;
            this.radID.CheckedChanged += new System.EventHandler(this.radID_CheckedChanged);
            // 
            // radLastName
            // 
            this.radLastName.AutoSize = true;
            this.radLastName.Location = new System.Drawing.Point(109, 11);
            this.radLastName.Name = "radLastName";
            this.radLastName.Size = new System.Drawing.Size(97, 21);
            this.radLastName.TabIndex = 1;
            this.radLastName.Text = "Last Name";
            this.radLastName.UseVisualStyleBackColor = true;
            this.radLastName.CheckedChanged += new System.EventHandler(this.radLastName_CheckedChanged);
            // 
            // radFirstName
            // 
            this.radFirstName.AutoSize = true;
            this.radFirstName.Checked = true;
            this.radFirstName.Location = new System.Drawing.Point(6, 10);
            this.radFirstName.Name = "radFirstName";
            this.radFirstName.Size = new System.Drawing.Size(97, 21);
            this.radFirstName.TabIndex = 0;
            this.radFirstName.TabStop = true;
            this.radFirstName.Text = "First Name";
            this.radFirstName.UseVisualStyleBackColor = true;
            this.radFirstName.CheckedChanged += new System.EventHandler(this.radFirstName_CheckedChanged);
            // 
            // txtSalaryTo
            // 
            this.txtSalaryTo.Location = new System.Drawing.Point(252, 135);
            this.txtSalaryTo.Name = "txtSalaryTo";
            this.txtSalaryTo.Size = new System.Drawing.Size(125, 22);
            this.txtSalaryTo.TabIndex = 34;
            // 
            // txtSalaryFrom
            // 
            this.txtSalaryFrom.Location = new System.Drawing.Point(66, 135);
            this.txtSalaryFrom.Name = "txtSalaryFrom";
            this.txtSalaryFrom.Size = new System.Drawing.Size(125, 22);
            this.txtSalaryFrom.TabIndex = 32;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(186, 137);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(57, 17);
            this.lblTo.TabIndex = 33;
            this.lblTo.Text = "       To:";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(6, 138);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(44, 17);
            this.lblFrom.TabIndex = 31;
            this.lblFrom.Text = "From:";
            // 
            // btnSearchClear
            // 
            this.btnSearchClear.Location = new System.Drawing.Point(450, 137);
            this.btnSearchClear.Name = "btnSearchClear";
            this.btnSearchClear.Size = new System.Drawing.Size(85, 53);
            this.btnSearchClear.TabIndex = 25;
            this.btnSearchClear.Text = "Clear";
            this.btnSearchClear.UseVisualStyleBackColor = true;
            this.btnSearchClear.Click += new System.EventHandler(this.btnSearchClear_Click);
            // 
            // timeSearchDate
            // 
            this.timeSearchDate.Checked = false;
            this.timeSearchDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.timeSearchDate.Location = new System.Drawing.Point(66, 168);
            this.timeSearchDate.Name = "timeSearchDate";
            this.timeSearchDate.Size = new System.Drawing.Size(311, 22);
            this.timeSearchDate.TabIndex = 24;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(6, 171);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(42, 17);
            this.lblDate.TabIndex = 13;
            this.lblDate.Text = "Date:";
            // 
            // lblSkills
            // 
            this.lblSkills.AutoSize = true;
            this.lblSkills.Location = new System.Drawing.Point(4, 103);
            this.lblSkills.Name = "lblSkills";
            this.lblSkills.Size = new System.Drawing.Size(44, 17);
            this.lblSkills.TabIndex = 10;
            this.lblSkills.Text = "Skills:";
            // 
            // cmbSkills
            // 
            this.cmbSkills.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSkills.FormattingEnabled = true;
            this.cmbSkills.Location = new System.Drawing.Point(66, 101);
            this.cmbSkills.Name = "cmbSkills";
            this.cmbSkills.Size = new System.Drawing.Size(311, 24);
            this.cmbSkills.TabIndex = 9;
            // 
            // btnSearchGo
            // 
            this.btnSearchGo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSearchGo.Location = new System.Drawing.Point(450, 69);
            this.btnSearchGo.Name = "btnSearchGo";
            this.btnSearchGo.Size = new System.Drawing.Size(85, 62);
            this.btnSearchGo.TabIndex = 7;
            this.btnSearchGo.Text = "Go!";
            this.btnSearchGo.UseVisualStyleBackColor = true;
            this.btnSearchGo.Click += new System.EventHandler(this.btnSearchGo_Click);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(3, 72);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(57, 17);
            this.lblSearch.TabIndex = 6;
            this.lblSearch.Text = "Search:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(66, 69);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(311, 22);
            this.txtSearch.TabIndex = 4;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // addPhotoToolStripMenuItem
            // 
            this.addPhotoToolStripMenuItem.Name = "addPhotoToolStripMenuItem";
            this.addPhotoToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // changePhotoToolStripMenuItem
            // 
            this.changePhotoToolStripMenuItem.Name = "changePhotoToolStripMenuItem";
            this.changePhotoToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // deletToolStripMenuItem
            // 
            this.deletToolStripMenuItem.Name = "deletToolStripMenuItem";
            this.deletToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // pictureBoxEmp
            // 
            this.pictureBoxEmp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxEmp.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBoxEmp.ErrorImage = null;
            this.pictureBoxEmp.Location = new System.Drawing.Point(933, 66);
            this.pictureBoxEmp.Name = "pictureBoxEmp";
            this.pictureBoxEmp.Size = new System.Drawing.Size(84, 126);
            this.pictureBoxEmp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEmp.TabIndex = 23;
            this.pictureBoxEmp.TabStop = false;
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stpAdd,
            this.stpEdit,
            this.stpDelet,
            this.stpHelp,
            this.stpAbout});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1038, 28);
            this.menuStrip.TabIndex = 24;
            this.menuStrip.Text = "menuStrip";
            // 
            // stpAdd
            // 
            this.stpAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stpNewEmp,
            this.stpNewSkill});
            this.stpAdd.Name = "stpAdd";
            this.stpAdd.Size = new System.Drawing.Size(49, 24);
            this.stpAdd.Text = "Add";
            // 
            // stpNewEmp
            // 
            this.stpNewEmp.Name = "stpNewEmp";
            this.stpNewEmp.Size = new System.Drawing.Size(178, 24);
            this.stpNewEmp.Text = "New Employee";
            this.stpNewEmp.Click += new System.EventHandler(this.stpNewEmp_Click);
            // 
            // stpNewSkill
            // 
            this.stpNewSkill.Name = "stpNewSkill";
            this.stpNewSkill.Size = new System.Drawing.Size(178, 24);
            this.stpNewSkill.Text = "New Skill";
            this.stpNewSkill.Click += new System.EventHandler(this.stpNewSkill_Click);
            // 
            // stpEdit
            // 
            this.stpEdit.Name = "stpEdit";
            this.stpEdit.Size = new System.Drawing.Size(47, 24);
            this.stpEdit.Text = "Edit";
            this.stpEdit.Click += new System.EventHandler(this.stpEdit_Click);
            // 
            // stpDelet
            // 
            this.stpDelet.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stpDeletEmployee,
            this.stpDeletAllEmp,
            this.stpDeletAllSkills});
            this.stpDelet.Name = "stpDelet";
            this.stpDelet.Size = new System.Drawing.Size(57, 24);
            this.stpDelet.Text = "Delet";
            // 
            // stpDeletEmployee
            // 
            this.stpDeletEmployee.Name = "stpDeletEmployee";
            this.stpDeletEmployee.Size = new System.Drawing.Size(200, 24);
            this.stpDeletEmployee.Text = "Selected Rows";
            this.stpDeletEmployee.Click += new System.EventHandler(this.stpDeletEmployee_Click);
            // 
            // stpDeletAllEmp
            // 
            this.stpDeletAllEmp.Name = "stpDeletAllEmp";
            this.stpDeletAllEmp.Size = new System.Drawing.Size(200, 24);
            this.stpDeletAllEmp.Text = "All The Employees";
            this.stpDeletAllEmp.Click += new System.EventHandler(this.stpDeletAllEmp_Click);
            // 
            // stpDeletAllSkills
            // 
            this.stpDeletAllSkills.Name = "stpDeletAllSkills";
            this.stpDeletAllSkills.Size = new System.Drawing.Size(200, 24);
            this.stpDeletAllSkills.Text = "All The Skills";
            this.stpDeletAllSkills.Click += new System.EventHandler(this.stpDeletAllSkills_Click);
            // 
            // stpHelp
            // 
            this.stpHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stpExit});
            this.stpHelp.Name = "stpHelp";
            this.stpHelp.Size = new System.Drawing.Size(53, 24);
            this.stpHelp.Text = "Help";
            // 
            // stpExit
            // 
            this.stpExit.Name = "stpExit";
            this.stpExit.Size = new System.Drawing.Size(102, 24);
            this.stpExit.Text = "Exit";
            this.stpExit.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // stpAbout
            // 
            this.stpAbout.Name = "stpAbout";
            this.stpAbout.Size = new System.Drawing.Size(62, 24);
            this.stpAbout.Text = "About";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(666, 466);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "Skill Info:";
            // 
            // lblTotalSkills
            // 
            this.lblTotalSkills.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblTotalSkills.Location = new System.Drawing.Point(221, 12);
            this.lblTotalSkills.Name = "lblTotalSkills";
            this.lblTotalSkills.Size = new System.Drawing.Size(119, 22);
            this.lblTotalSkills.TabIndex = 28;
            this.lblTotalSkills.Text = "Total Skills";
            // 
            // lblTotalSalaries
            // 
            this.lblTotalSalaries.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblTotalSalaries.Location = new System.Drawing.Point(428, 13);
            this.lblTotalSalaries.Name = "lblTotalSalaries";
            this.lblTotalSalaries.Size = new System.Drawing.Size(218, 17);
            this.lblTotalSalaries.TabIndex = 27;
            this.lblTotalSalaries.Text = "Total Salaries";
            // 
            // lblTotalEmp
            // 
            this.lblTotalEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblTotalEmp.Location = new System.Drawing.Point(4, 13);
            this.lblTotalEmp.Name = "lblTotalEmp";
            this.lblTotalEmp.Size = new System.Drawing.Size(147, 17);
            this.lblTotalEmp.TabIndex = 25;
            this.lblTotalEmp.Text = "Total Employess";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblTotalEmp);
            this.groupBox1.Controls.Add(this.lblTotalSalaries);
            this.groupBox1.Controls.Add(this.lblTotalSkills);
            this.groupBox1.Location = new System.Drawing.Point(11, 466);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(652, 36);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // richTxtBox_Skill
            // 
            this.richTxtBox_Skill.BackColor = System.Drawing.SystemColors.HighlightText;
            this.richTxtBox_Skill.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTxtBox_Skill.Location = new System.Drawing.Point(669, 486);
            this.richTxtBox_Skill.Name = "richTxtBox_Skill";
            this.richTxtBox_Skill.ReadOnly = true;
            this.richTxtBox_Skill.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTxtBox_Skill.ShortcutsEnabled = false;
            this.richTxtBox_Skill.Size = new System.Drawing.Size(357, 250);
            this.richTxtBox_Skill.TabIndex = 30;
            this.richTxtBox_Skill.Text = "";
            this.richTxtBox_Skill.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTxtBox_Skill_LinkClicked);
            this.richTxtBox_Skill.Enter += new System.EventHandler(this.richTxtBox_Skill_Enter);
            // 
            // richTxtBox_Emp
            // 
            this.richTxtBox_Emp.BackColor = System.Drawing.SystemColors.HighlightText;
            this.richTxtBox_Emp.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTxtBox_Emp.Location = new System.Drawing.Point(669, 60);
            this.richTxtBox_Emp.Name = "richTxtBox_Emp";
            this.richTxtBox_Emp.ReadOnly = true;
            this.richTxtBox_Emp.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTxtBox_Emp.ShortcutsEnabled = false;
            this.richTxtBox_Emp.Size = new System.Drawing.Size(357, 403);
            this.richTxtBox_Emp.TabIndex = 32;
            this.richTxtBox_Emp.Text = "";
            this.richTxtBox_Emp.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTxtBox_Emp_LinkClicked);
            this.richTxtBox_Emp.Enter += new System.EventHandler(this.richTxtBox_Emp_Enter);
            // 
            // tabControlEmpInfo
            // 
            this.tabControlEmpInfo.Controls.Add(this.tabEmpInfo);
            this.tabControlEmpInfo.Controls.Add(this.tabCV);
            this.tabControlEmpInfo.Location = new System.Drawing.Point(669, 36);
            this.tabControlEmpInfo.Name = "tabControlEmpInfo";
            this.tabControlEmpInfo.SelectedIndex = 0;
            this.tabControlEmpInfo.Size = new System.Drawing.Size(357, 427);
            this.tabControlEmpInfo.TabIndex = 0;
            this.tabControlEmpInfo.SelectedIndexChanged += new System.EventHandler(this.tabControlEmpInfo_SelectedIndexChanged);
            // 
            // tabEmpInfo
            // 
            this.tabEmpInfo.Location = new System.Drawing.Point(4, 25);
            this.tabEmpInfo.Name = "tabEmpInfo";
            this.tabEmpInfo.Size = new System.Drawing.Size(349, 398);
            this.tabEmpInfo.TabIndex = 0;
            this.tabEmpInfo.Text = "Employee Info";
            this.tabEmpInfo.UseVisualStyleBackColor = true;
            // 
            // tabCV
            // 
            this.tabCV.Location = new System.Drawing.Point(4, 25);
            this.tabCV.Name = "tabCV";
            this.tabCV.Size = new System.Drawing.Size(349, 398);
            this.tabCV.TabIndex = 0;
            this.tabCV.Text = "CV";
            this.tabCV.UseVisualStyleBackColor = true;
            // 
            // FormHR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 751);
            this.Controls.Add(this.richTxtBox_Skill);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.pictureBoxEmp);
            this.Controls.Add(this.grpSearchEmp);
            this.Controls.Add(this.tabControlGrds);
            this.Controls.Add(this.richTxtBox_Emp);
            this.Controls.Add(this.tabControlEmpInfo);
            this.MaximizeBox = false;
            this.Name = "FormHR";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HR Database";
            this.Load += new System.EventHandler(this.FormHR_Load);
            this.tabControlGrds.ResumeLayout(false);
            this.tabAllEmp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdEmp)).EndInit();
            this.tabAllSkills.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdSkl)).EndInit();
            this.grpSearchEmp.ResumeLayout(false);
            this.grpSearchEmp.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmp)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabControlEmpInfo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabAllEmp;
        private System.Windows.Forms.GroupBox grpSearchEmp;
        private System.Windows.Forms.Label lblSkills;
        private System.Windows.Forms.ComboBox cmbSkills;
        private System.Windows.Forms.Button btnSearchGo;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.RadioButton radSkill;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.RadioButton radEmail;
        private System.Windows.Forms.RadioButton radID;
        private System.Windows.Forms.RadioButton radLastName;
        private System.Windows.Forms.RadioButton radFirstName;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addPhotoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePhotoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deletToolStripMenuItem;
        private System.Windows.Forms.RadioButton radDate;
        private System.Windows.Forms.DateTimePicker timeSearchDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.PictureBox pictureBoxEmp;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem stpAdd;
        private System.Windows.Forms.ToolStripMenuItem stpNewEmp;
        private System.Windows.Forms.ToolStripMenuItem stpNewSkill;
        private System.Windows.Forms.ToolStripMenuItem stpEdit;
        private System.Windows.Forms.ToolStripMenuItem stpDelet;
        private System.Windows.Forms.ToolStripMenuItem stpHelp;
        private System.Windows.Forms.ToolStripMenuItem stpExit;
        private System.Windows.Forms.ToolStripMenuItem stpAbout;
        private System.Windows.Forms.TabPage tabAllSkills;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTotalSkills;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.DataGridView grdEmp;
        public System.Windows.Forms.Label lblTotalSalaries;
        public System.Windows.Forms.Label lblTotalEmp;
        private System.Windows.Forms.ToolStripMenuItem stpDeletEmployee;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblID;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblSkills;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblSalary;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblEndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblGander;
        private System.Windows.Forms.ToolStripMenuItem stpDeletAllEmp;
        private System.Windows.Forms.ToolStripMenuItem stpDeletAllSkills;
        public System.Windows.Forms.DataGridView grdSkl;
        private System.Windows.Forms.Button btnSearchClear;
        private System.Windows.Forms.RichTextBox richTxtBox_Skill;
        private System.Windows.Forms.RichTextBox richTxtBox_Emp;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblSkillsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblSkillDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblSkillLinq;
        private System.Windows.Forms.TabControl tabControlEmpInfo;
        private System.Windows.Forms.TabPage tabCV;
        private System.Windows.Forms.TabPage tabEmpInfo;
        public System.Windows.Forms.TabControl tabControlGrds;
        private System.Windows.Forms.RadioButton radSalary;
        private System.Windows.Forms.TextBox txtSalaryTo;
        private System.Windows.Forms.TextBox txtSalaryFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

