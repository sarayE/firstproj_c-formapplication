﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class AddSkill : Form
    {
        FormHR originalForm;
        public int firstEditBarcode;

        public AddSkill(FormHR incomingForm)
        {
            originalForm = incomingForm;
            InitializeComponent();
        }

        //Submit:
        private void btnSubmitSkill_Click(object sender, EventArgs e)
        {
            #region Add New Skill - Submit
            if (originalForm.itsNewEmpOrEdit == "ImstpNewSkl")
            {
                if ((!string.IsNullOrEmpty(this.txtBarcode.Text)) && (!string.IsNullOrEmpty(this.txtSkillName.Text)) && (!string.IsNullOrEmpty(this.txtDescription.Text)))
                {
                    string skillName = this.txtSkillName.Text;
                    string description = this.txtDescription.Text;
                    string linkSkillInfo = this.txtAddLink.Text;
                    int barcode;
                    if (int.TryParse(this.txtBarcode.Text, out barcode) == true)
                    {
                        if (originalForm.lstSkills.Exists(x => x.Barcode == barcode))
                        {
                            MessageBox.Show("The Addition Failed!\n(There is already a skill with this Barcode)");
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(linkSkillInfo) || IsValidLink(linkSkillInfo))
                            {
                                if (MessageBox.Show(string.Format("Are you sure do you want to Add \"{0} (Barcode: {1})\" ?", skillName, barcode), "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    originalForm.lstSkills.Add(new Skills(barcode, skillName)
                                    {
                                        Description = description,
                                        LinkSkillInfo = linkSkillInfo,
                                    });

                                    originalForm.grdSkl.Rows.Clear();
                                    originalForm.PrintGrdSkl();
                                    MessageBox.Show("Succeeded!");
                                    this.Close();
                                }
                            }
                            else
                            {
                                MessageBox.Show("The link address is invalid\n(Format: http://xxx.xx)");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("The Barcode must contain only numbers");
                    }
                }
                else
                {
                    this.ChekedIfIsNullOrEmpty();
                }
            }

            #endregion

            #region Edit Emp - Submit
            else if (originalForm.itsNewEmpOrEdit == "ImstpEdit") ///////////////EDIT-Submit!!!
            {
                if ((!string.IsNullOrEmpty(this.txtBarcode.Text)) && (!string.IsNullOrEmpty(this.txtSkillName.Text)) && (!string.IsNullOrEmpty(this.txtDescription.Text)))
                {
                    string skillName = this.txtSkillName.Text;
                    string description = this.txtDescription.Text;
                    string linkSkillInfo = this.txtAddLink.Text;
                    int barcode;
                    if (int.TryParse(this.txtBarcode.Text, out barcode) == true)
                    {
                        if (barcode != firstEditBarcode)
                        {
                            if (originalForm.lstSkills.Exists(x => x.Barcode == barcode))
                            {
                                MessageBox.Show("The Addition Failed!\n(There is already a skill with this Barcode)");
                            }
                            else
                            {
                                #region Are you sure do you want to Edit?
                                if (string.IsNullOrEmpty(linkSkillInfo) || IsValidLink(linkSkillInfo))
                                {
                                    if (MessageBox.Show(string.Format("Are you sure do you want to Edit \"{0} (Barcode: {1})\" ?", skillName, barcode), "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                    {
                                        originalForm.lstSkills.Add(new Skills(barcode, skillName)
                                        {
                                            Description = description,
                                            LinkSkillInfo = linkSkillInfo,
                                        });

                                        int index = (originalForm.lstSkills.FindIndex(x => x.Barcode == firstEditBarcode));
                                        originalForm.lstSkills.RemoveAt(index);///////remove old one
                                        originalForm.grdSkl.Rows.Clear();
                                        originalForm.PrintGrdSkl();
                                        MessageBox.Show("Succeeded!");
                                        this.Close();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("The link address is invalid\n(Format: http://xxx.xx)");
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            #region Are you sure do you want to Edit?
                            if (string.IsNullOrEmpty(linkSkillInfo) || IsValidLink(linkSkillInfo))
                            {
                                if (MessageBox.Show(string.Format("Are you sure do you want to Edit \"{0} (Barcode: {1})\" ?", skillName, barcode), "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    originalForm.lstSkills.Add(new Skills(barcode, skillName)
                                    {
                                        Description = description,
                                        LinkSkillInfo = linkSkillInfo,
                                    });

                                    int index = (originalForm.lstSkills.FindIndex(x => x.Barcode == firstEditBarcode));
                                    originalForm.lstSkills.RemoveAt(index);///////remove old one
                                    originalForm.grdSkl.Rows.Clear();
                                    originalForm.PrintGrdSkl();
                                    MessageBox.Show("Succeeded!");
                                    this.Close();
                                }
                            }
                            else
                            {
                                MessageBox.Show("The link address is invalid\n(Format: http://xxx.xx)");
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        MessageBox.Show("The Barcode must contain only numbers");
                    }
                }
                else
                {
                    this.ChekedIfIsNullOrEmpty();
                }
            }
            #endregion
        }
        
        
        //Exception if the addition text-> isNullOrEmpty:
        private void ChekedIfIsNullOrEmpty()
        {
            if ((string.IsNullOrEmpty(this.txtBarcode.Text)) && (!string.IsNullOrEmpty(this.txtSkillName.Text)) && (!string.IsNullOrEmpty(this.txtDescription.Text)))
            {
                MessageBox.Show("You must enter a Barcode");
            }
            else if ((string.IsNullOrEmpty(this.txtBarcode.Text)) && (string.IsNullOrEmpty(this.txtSkillName.Text)) && (!string.IsNullOrEmpty(this.txtDescription.Text)))
            {
                MessageBox.Show("You must enter a Barcode and a Skill Name");
            }
            else if ((string.IsNullOrEmpty(this.txtBarcode.Text)) && (!string.IsNullOrEmpty(this.txtSkillName.Text)) && (string.IsNullOrEmpty(this.txtDescription.Text)))
            {
                MessageBox.Show("You must enter a Barcode and a Description");
            }
            else if ((!string.IsNullOrEmpty(this.txtBarcode.Text)) && (string.IsNullOrEmpty(this.txtSkillName.Text)) && (string.IsNullOrEmpty(this.txtDescription.Text)))
            {
                MessageBox.Show("You must enter a Skill Name and a Description");
            }
            else if ((!string.IsNullOrEmpty(this.txtBarcode.Text)) && (!string.IsNullOrEmpty(this.txtSkillName.Text)) && (string.IsNullOrEmpty(this.txtDescription.Text)))
            {
                MessageBox.Show("You must enter a Description");
            }
            else if ((!string.IsNullOrEmpty(this.txtBarcode.Text)) && (string.IsNullOrEmpty(this.txtSkillName.Text)) && (!string.IsNullOrEmpty(this.txtDescription.Text)))
            {
                MessageBox.Show("You must enter a Skill Name");
            }
            else if ((string.IsNullOrEmpty(this.txtBarcode.Text)) && (string.IsNullOrEmpty(this.txtSkillName.Text)) && (string.IsNullOrEmpty(this.txtDescription.Text)))
            {
                MessageBox.Show("You must enter a Barcode, Skill Name and a Description");
            }
        }


        //Link:
        private bool IsValidLink(string addlink)
        {
            try
            {
                Uri link = new Uri(addlink);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
