﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class Skills
    {
        public Skills(int barcode, string skillName)
        {
            this.Barcode = barcode;
            this.SkillName = skillName;
        }

        public int Barcode { get; set; }
        public string SkillName { get; set; }
        public string Description { get; set; }
        public string LinkSkillInfo { get; set; }
        

        public override string ToString()
        {
            return string.Format("Skill Name: {0} \nBrief Description: {1} \n\nLinq To More Information: {2}",
                                  SkillName, Description, LinkSkillInfo);
        }

        public string PrintSkillName()
        {
            return String.Format("\n({0}) ", SkillName);
        }


    }
}
