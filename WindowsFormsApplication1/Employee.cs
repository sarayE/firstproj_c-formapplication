﻿using System;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public enum Gender
    {
        Male,
        Female
    }

    public class Employee
    {
        public Employee(string firstName, string lastName, int id, Gender gender)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.ID = id;
            this.GenderEmp = gender;
        }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ID { get; set; }
        public Gender GenderEmp { get; set; }
        public int Salary { get; set; }
        public string Email { get; set; }
        public DateTime StartToWorkAt { get; set; }
        public string Skill { get; set; }
        public string Image { get; set; }
        public string CV { get; set; }

        public string PrintStartWork()
        {
            return String.Format("{0}/{1}/{2}", StartToWorkAt.Day, StartToWorkAt.Month, StartToWorkAt.Year);
        }
    }
}
