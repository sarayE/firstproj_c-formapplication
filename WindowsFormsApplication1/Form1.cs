﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public partial class FormHR : Form
    {
        public string itsNewEmpOrEdit = "empty";
        internal List<Skills> lstSkills;
        internal List<Employee> lstEmps;

        public FormHR()
        {
            InitializeComponent();

            #region start rad
            this.lblSkills.Enabled = false;
            this.cmbSkills.Enabled = false;
            this.lblDate.Enabled = false;
            this.timeSearchDate.Enabled = false;
            this.stpDeletAllSkills.Enabled = false;
            this.lblFrom.Enabled = false;
            this.lblTo.Enabled = false;
            this.txtSalaryFrom.Enabled = false;
            this.txtSalaryTo.Enabled = false;
            #endregion
        }

        private void FormHR_Load(object sender, EventArgs e)
        {
            #region lstSkills

            //Skill List:
            this.lstSkills = new List<Skills>();

            this.lstSkills.Add(new Skills(111, "ADO.NET 2.0")
            {
                Description = "ADO.NET provides consistent access to data sources such as SQL Server and XML, and to data sources exposed through OLE DB and ODBC.",
                LinkSkillInfo = @"https://msdn.microsoft.com/en-us/library/h43ks021(v=vs.110).aspx"
            });
            this.lstSkills.Add(new Skills(222, "ISP.NET")
            {
                Description = "ISP.NET is a code level verification tool for MPI programs. It includes a Visual Studio 2010 extension that allows for push button verification of user programs that are written in C, C++ and C#. ISP checks for deadlocks, assertion violations, and other MPI program issues.",
                LinkSkillInfo = @"http://isp.codeplex.com/"
            });
            this.lstSkills.Add(new Skills(333, "C# 6.0")
            {
                Description = "C# is a type-safe, object-oriented language that is simple yet powerful, allowing programmers to build a breadth of applications. Combined with the .NET Framework, Visual C# enables the creation of Windows applications, Web services, database tools, components, controls, and more.",
                LinkSkillInfo = @"https://en.wikipedia.org/wiki/C_Sharp_(programming_language)"
            });
            this.lstSkills.Add(new Skills(444, "java SE 8.0")
            {
                Description = "Java Platform, Standard Edition (Java SE) lets you develop and deploy Java applications on desktops and servers, as well as in today's demanding embedded environments. Java offers the rich user interface, performance, versatility, portability, and security that today's applicationsrequire.",
                LinkSkillInfo = @"http://www.oracle.com/technetwork/java/javase/overview/index.html"
            });
            this.lstSkills.Add(new Skills(555, "API")
            {
                Description = "Computer programming, an application programming interface (API) is a set of routines, protocols, and tools for building software applications.",
                LinkSkillInfo = @"https://en.wikipedia.org/wiki/Application_programming_interface"
            });
            this.lstSkills.Add(new Skills(666, "ASP.NET 5")
            {
                Description = " ASP.NET 5. is a development framework for building web pages and web sites with HTML, CSS, JavaScript and server scripting.",
                LinkSkillInfo = @"http://www.asp.net/"
            });


            #endregion

            this.PrintGrdSkl();



            #region lstEmp

            //Employee List:
            this.lstEmps = new List<Employee>();

            this.lstEmps.Add(new Employee("Raz", "Choen", 0001, Gender.Male)
            {
                Skill = lstSkills[1].PrintSkillName() + lstSkills[2].PrintSkillName(),
                Salary = 6700,
                Email = "Raz@bezeqint.net",
                StartToWorkAt = new DateTime(2010, 3, 26),
                Image = @"http://www.adigoldberg.co.il/zebra_co/uploads/2012/06/%D7%AA%D7%9E%D7%95%D7%A0%D7%AA-%D7%A4%D7%A1%D7%A4%D7%95%D7%A8%D7%981.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\RazChoen.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\RazChoenCV.txt",
            });
            this.lstEmps.Add(new Employee("Tomi", "Klimoner", 0002, Gender.Male)
            {
                Skill = lstSkills[0].PrintSkillName() + lstSkills[1].PrintSkillName() + lstSkills[2].PrintSkillName(),
                Salary = 8900,
                Email = "Tomi@walla.co.il",
                StartToWorkAt = new DateTime(2005, 4, 23),
                Image = @"http://4.bp.blogspot.com/-9keed5ihmG8/TkL4w3k2JZI/AAAAAAAASu0/UW-axGnVH40/s1600/9821_RubiArt_by_Peter_Frank_dk.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\TomiKlimoner.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\TomiKlimonerCV.txt",
            });
            this.lstEmps.Add(new Employee("Saray", "Eliyahu", 0003, Gender.Female)
            {
                Skill = lstSkills[2].PrintSkillName(),
                Salary = 6100,
                Email = "SarayEliyahu@gmail.com",
                StartToWorkAt = new DateTime(2014, 2, 22),
                Image = @"http://cdn.glamasia.com/wp-content/uploads/2013/11/fresh-face-224x300.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\SarayEliyahu.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\SarayEliyahuCV.txt",
            });
            this.lstEmps.Add(new Employee("Tal", "Moris", 0004, Gender.Female)
            {
                Skill = lstSkills[5].PrintSkillName(),
                Salary = 6020,
                Email = "TalMo@gmail.com",
                StartToWorkAt = new DateTime(2015, 1, 18),
                Image = @"https://c1.staticflickr.com/9/8070/8263882333_cd10e5c900_b.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\TalMoris.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\TalMorisCV.txt",
            });
            this.lstEmps.Add(new Employee("Michal", "Wilo", 0005, Gender.Female)
            {
                Skill = lstSkills[0].PrintSkillName() + lstSkills[1].PrintSkillName(),
                Salary = 5400,
                Email = "MichalWi@gmail.com",
                StartToWorkAt = new DateTime(2013, 5, 23),
                Image = @"http://www.tasherstudio.com/_images//tasherphotos700/2010_0213canada0002fx.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\MichalWilo.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\MichalWiloCV.txt",
            });
            this.lstEmps.Add(new Employee("Boris", "Tregan", 0006, Gender.Male)
            {
                Skill = lstSkills[3].PrintSkillName(),
                Salary = 3300,
                Email = "Boris@walla.com",
                StartToWorkAt = new DateTime(2015, 6, 1),
                Image = @"http://www.postofficeholiday.co.uk/sites/all/files/styles/large/public/01._neutral_0.jpg?itok=ScSO3TSi",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\BorisTregan.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\BorisTreganCV.txt",
            });
            this.lstEmps.Add(new Employee("Nava", "Gan", 0007, Gender.Female)
            {
                Skill = lstSkills[1].PrintSkillName() + lstSkills[5].PrintSkillName() + lstSkills[0].PrintSkillName(),
                Salary = 7100,
                Email = "NavaGan@bezeqint.net",
                StartToWorkAt = new DateTime(2003, 10, 13),
                Image = @"http://www.careers.gov.sg/images/default-source/intern's-bulletin/passport.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\NavaGan.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\NavaGanCV.txt",
            });
            this.lstEmps.Add(new Employee("Leon", "Doson", 0008, Gender.Male)
            {
                Skill = lstSkills[0].PrintSkillName() + lstSkills[2].PrintSkillName(),
                Salary = 4200,
                Email = "Leon@zahav.com",
                StartToWorkAt = new DateTime(2014, 12, 23),
                Image = @"http://moroccoworldnews.com/wp-content/uploads/2011/06/Ahmad_passport-photo.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\LeonDoson.jpg",
            });
            this.lstEmps.Add(new Employee("Daniel", "Roye", 0009, Gender.Male)
            {
                Skill = lstSkills[4].PrintSkillName() + lstSkills[3].PrintSkillName(),
                Salary = 5500,
                Email = "Danir@bezeqint.net",
                StartToWorkAt = new DateTime(2010, 7, 7),
                Image = @"https://photoflex.com/images/uploads/PLS/Do%20It%20Yourself%20Passport%20Photos%20with%20the%20First%20Studio%20Portrait%20Kit/1229712977_final2.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\DanielRoye.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\DanielRoyeCV.txt",
            });
            this.lstEmps.Add(new Employee("Yakov", "Levy", 0010, Gender.Male)
            {
                Skill = lstSkills[1].PrintSkillName(),
                Salary = 3090,
                Email = "YakovLevy@zahav.co.il",
                StartToWorkAt = new DateTime(2011, 9, 16),
                Image = @"http://images2.lawguide.co.il/upload/01%20%D7%A4%D7%A1%D7%A4%D7%95%D7%A8%D7%98.JPG",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\YakovLevy.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\YakovLevyCV.txt",
            });
            this.lstEmps.Add(new Employee("Sharona", "Levin", 0011, Gender.Female)
            {
                Skill = lstSkills[4].PrintSkillName() + lstSkills[2].PrintSkillName(),
                Salary = 6300,
                Email = "Sharona@gmail.com",
                StartToWorkAt = new DateTime(2009, 9, 18),
                Image = @"https://avitalrothzaidel.files.wordpress.com/2010/12/d7aad79ed795d7a0d7aa-d7a4d7a1d7a4d795d7a8d798-e1291547271980.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\SharonaLevin.jpg",
            });
            this.lstEmps.Add(new Employee("Yamit", "Eliya", 0012, Gender.Female)
            {
                Skill = lstSkills[2].PrintSkillName(),
                Salary = 2040,
                Email = "Yamit@gmail.com",
                StartToWorkAt = new DateTime(2002, 7, 14),
                Image = @"http://www.passportphotoaz.com/wp-content/uploads/2010/03/costa-rica-passport-photo-235x300.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\YamitEliya.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\YamitEliyaCV.txt",
            });
            this.lstEmps.Add(new Employee("Erez", "Tal", 0013, Gender.Male)
            {
                Skill = lstSkills[1].PrintSkillName() + lstSkills[3].PrintSkillName(),
                Salary = 5820,
                Email = "Erez@bezeqint.net",
                StartToWorkAt = new DateTime(2015, 9, 12),
                Image = @"http://www.amalnet.k12.il/NR/rdonlyres/8874C06E-1D29-4843-8C11-180FA7027006/14275/%D7%A4%D7%A1%D7%A4%D7%95%D7%A8%D7%98.JPG",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\ErezTal.jpg",
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\ErezTalCV.txt",
            });
            this.lstEmps.Add(new Employee("Dvir", "Meler", 0014, Gender.Male)
            {
                Skill = lstSkills[0].PrintSkillName() + lstSkills[2].PrintSkillName(),
                Salary = 4200,
                Email = "Dvir@zahav.com",
                StartToWorkAt = new DateTime(2010, 5, 7),
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\DvirMelerCV.txt",

            });
            this.lstEmps.Add(new Employee("Yarin", "Hof", 0015, Gender.Male)
            {
                Skill = lstSkills[3].PrintSkillName() + lstSkills[1].PrintSkillName() + lstSkills[5].PrintSkillName(),
                Salary = 11260,
                Email = "Yarin@gmail.com",
                StartToWorkAt = new DateTime(2012, 9, 12),
                Image = @"http://humanities.tau.ac.il/history-school/sites/student_71/wp-content/uploads/sites/65/2014/10/%D7%AA%D7%9E%D7%95%D7%A0%D7%AA-%D7%A4%D7%A1%D7%A4%D7%95%D7%A8%D7%98.jpg",
                //Image = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\YarinHof.jpg",
            });
            this.lstEmps.Add(new Employee("Luba", "Noya", 0016, Gender.Female)
            {
                Skill = lstSkills[5].PrintSkillName() + lstSkills[2].PrintSkillName(),
                Salary = 7450,
                Email = "LubaNoya@gmail.com",
                StartToWorkAt = new DateTime(2000, 6, 5),
                CV = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesCV\LubaNoyaCV.txt",
            });
            #endregion

            this.PrintGrdEmp();

            //disable focus richTxtBox_Skill null or empty:
            this.richTxtBox_Skill.Enter += new EventHandler(richTxtBox_Skill_Enter);
        }


        public void PrintGrdEmp()
        {
            //Print Employee to the table + (Total Employees + Salary)
            int salaryCounter = 0;
            foreach (Employee emp in this.lstEmps)
            {
                this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp); //Add to the grdEmp
                this.lblTotalEmp.Text = String.Format("Total Employees: {0}", this.lstEmps.Count.ToString());
                salaryCounter += emp.Salary;
                this.lblTotalSalaries.Text = String.Format("Total Salaries: {0}$", salaryCounter.ToString());
            }
        }
        public void PrintGrdSkl()
        {
            foreach (Skills skl in this.lstSkills)
            {
                this.grdSkl.Rows.Add(skl.Barcode, skl.SkillName, skl.Description, skl.LinkSkillInfo);
                this.lblTotalSkills.Text = String.Format("Total Skills: {0}", this.lstSkills.Count.ToString());
            }
        }
        public void PrintCV()
        {
            if (this.grdEmp.SelectedRows.Count > 0)
            {
                int id = int.Parse(this.grdEmp.SelectedRows[0].Cells["tblId"].Value.ToString());
                int index = (this.lstEmps.FindIndex(x => x.ID == id));

                if (string.IsNullOrEmpty(this.lstEmps[index].CV))
                {
                    this.richTxtBox_Emp.Text = "No CV Found.";
                }
                else
                {
                    this.richTxtBox_Emp.Text = File.ReadAllText(this.lstEmps[index].CV);
                }
            }
        }



        //Employee richTxtBox:
        private void grdEmp_SelectionChanged(object sender, EventArgs e)
        {
            if ((this.grdEmp.SelectedRows.Count > 0) && (this.tabControlEmpInfo.SelectedIndex == 0))
            {
                this.richTxtBox_Emp.Text = String.Format("Name: {1} {2}  -{0}- \n\nSkills: {3} \n\nSalary: {4}$ \n\nEmail:\n{5} \n\nStarted working at: {6}",
                    this.grdEmp.SelectedRows[0].Cells["tblID"].Value.ToString(),
                    this.grdEmp.SelectedRows[0].Cells["tblFirstName"].Value.ToString(),
                    this.grdEmp.SelectedRows[0].Cells["tblLastName"].Value.ToString(),
                    this.grdEmp.SelectedRows[0].Cells["tblSkills"].Value.ToString(),
                    this.grdEmp.SelectedRows[0].Cells["tblSalary"].Value.ToString(),
                    this.grdEmp.SelectedRows[0].Cells["tblEmail"].Value.ToString(),
                    this.grdEmp.SelectedRows[0].Cells["tblStartDate"].Value.ToString());

                if (this.grdEmp.SelectedRows[0].Cells["tblImage"].Value == null)//Genger Male or Female (show if no pic)
                {
                    if (this.grdEmp.SelectedRows[0].Cells["tblGander"].Value.Equals(Gender.Male))
                    {
                        this.pictureBoxEmp.ImageLocation = @"http://philosophy.ucr.edu/wp-content/uploads/2014/10/no-profile-img.gif";
                        //this.pictureBoxEmp.ImageLocation = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\MaleNoProfile.gif";
                    }
                    else
                    {
                        this.pictureBoxEmp.ImageLocation = @"http://www.danubeconsul.eu/female.jpg";
                        //this.pictureBoxEmp.ImageLocation = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\FemaleNoProfile.jpg";
                    }
                }
                else
                {
                    this.pictureBoxEmp.ImageLocation = this.grdEmp.SelectedRows[0].Cells[8].Value.ToString();
                }
            }
            else if (this.tabControlEmpInfo.SelectedIndex == 1)
            {
                this.PrintCV();
            }
        }

        //Skill richTxtBox:
        private void grdSkl_SelectionChanged(object sender, EventArgs e)
        {
            if (this.grdSkl.SelectedRows.Count > 0)
            {
                this.richTxtBox_Skill.Text = String.Format("Skill Name: {0} (Barcode: {1}) \n\nBrief Description: \n{2} \n\n\nTo more information: \n{3}",
                    this.grdSkl.SelectedRows[0].Cells["tblSkillsName"].Value.ToString(),
                    this.grdSkl.SelectedRows[0].Cells["tblBarcode"].Value.ToString(),
                    this.grdSkl.SelectedRows[0].Cells["tblSkillDescription"].Value.ToString(),
                    this.grdSkl.SelectedRows[0].Cells["tblSkillLinq"].Value.ToString());
            }
        }



        #region Delete
        //Delete:
        private void stpDeletEmployee_Click(object sender, EventArgs e)
        {
            if (this.tabControlGrds.SelectedIndex == 0)
            {
                int howManyLoops = this.lstEmps.Count;
                if (this.lstEmps.Count > 0)
                {
                    string namesToDelMessage = "";
                    for (int i = 0; i < howManyLoops; i++)
                    {
                        if (this.grdEmp.Rows[i].Selected == true)
                        {
                            string firstName = this.grdEmp.Rows[i].Cells["tblFirstName"].Value.ToString();
                            string lastName = this.grdEmp.Rows[i].Cells["tblLastName"].Value.ToString();

                            namesToDelMessage += "\n\"" + firstName + " " + lastName + "\"";
                        }
                    }

                    if (MessageBox.Show(string.Format("Are you sure do you want to Delet {0} ?", namesToDelMessage), "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        for (int i = 0; i < howManyLoops; i++)
                        {
                            if (this.grdEmp.Rows[i].Selected == true)
                            {
                                int idToDelet = int.Parse(this.grdEmp.Rows[i].Cells["tblId"].Value.ToString());

                                int index = (this.lstEmps.FindIndex(x => x.ID == idToDelet));
                                this.lstEmps.RemoveAt(index);
                            }
                        }
                        if (this.lstEmps.Count > 0)
                        {
                            this.grdEmp.Rows.Clear();
                            this.PrintGrdEmp();
                        }
                        else
                        {
                            this.grdEmp.Rows.Clear();
                            this.PrintGrdEmp();
                            this.richTxtBox_Emp.Text = null;
                            this.pictureBoxEmp.Image = null;
                            this.richTxtBox_Emp_Enter(sender, e);
                        }
                    }
                }
            }

            else if (this.tabControlGrds.SelectedIndex == 1)
            {
                int howManyLoops = this.lstSkills.Count;
                if (this.lstSkills.Count > 0)
                {
                    string namesToDelMessage = "";
                    for (int i = 0; i < howManyLoops; i++)
                    {
                        if (this.grdSkl.Rows[i].Selected == true)
                        {
                            string skillName = this.grdSkl.Rows[i].Cells["tblSkillsName"].Value.ToString();

                            namesToDelMessage += "\n\"" + skillName + "\"";
                        }
                    }

                    if (MessageBox.Show(string.Format("Are you sure do you want to Delet {0} ?", namesToDelMessage), "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        for (int i = 0; i < howManyLoops; i++)
                        {
                            if (this.grdSkl.Rows[i].Selected == true)
                            {
                                int barcodeToDelet = int.Parse(this.grdSkl.Rows[i].Cells["tblBarcode"].Value.ToString());

                                int index = (this.lstSkills.FindIndex(x => x.Barcode == barcodeToDelet));
                                this.lstSkills.RemoveAt(index);
                            }
                        }
                        if (this.lstSkills.Count > 0)
                        {
                            this.grdSkl.Rows.Clear();
                            this.PrintGrdSkl();
                        }
                        else
                        {
                            this.grdSkl.Rows.Clear();
                            this.PrintGrdSkl();
                            this.richTxtBox_Skill.Text = null;
                            this.richTxtBox_Skill_Enter(sender, e);
                        }
                    }
                }
            }


        }   // By Rows
        private void stpDeletAllEmp_Click(object sender, EventArgs e)
        {
            if (this.lstEmps.Count > 0)
            {
                if (MessageBox.Show("Are you sure do you want to Delet All of the Employees ?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.lstEmps.Clear();
                    this.grdEmp.Rows.Clear();
                    this.PrintGrdEmp();
                    this.richTxtBox_Emp.Text = null;
                    this.pictureBoxEmp.Image = null;
                }
                this.richTxtBox_Emp_Enter(sender, e);
            }
        }     // All Emps
        private void stpDeletAllSkills_Click(object sender, EventArgs e)
        {
            if (this.lstSkills.Count > 0)
            {
                if (MessageBox.Show("Are you sure do you want to Delet All of the Skills ?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.lstSkills.Clear();
                    this.grdSkl.Rows.Clear();
                    this.PrintGrdSkl();
                    this.richTxtBox_Skill.Text = null;
                }
                this.richTxtBox_Skill_Enter(sender, e);
            }
        }   // All Skills
        //Selected Index Changed -tabControlGrd- Delet.Enabled (Choose just: All Emp or All Skill)
        private void tabControlGrds_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControlGrds.SelectedIndex == 0)
            {
                this.stpDeletAllSkills.Enabled = false;
                this.stpDeletAllEmp.Enabled = true;
                this.grpSearchEmp.Enabled = true;
            }
            else if (this.tabControlGrds.SelectedIndex == 1)
            {
                this.stpDeletAllSkills.Enabled = true;
                this.stpDeletAllEmp.Enabled = false;
                this.grpSearchEmp.Enabled = false;
            }
        }
        #endregion

        #region richTxtBox Link(Skill) + Focus
        //richTxtBox_Skill links is clikable:
        private void richTxtBox_Skill_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }
        //disable focus richTxtBox_Skill null or empty:
        private void richTxtBox_Skill_Enter(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.richTxtBox_Skill.Text))
            {
                SendKeys.Send("{Tab}");
            }
        }
        //disable focus richTxtBox_Emp null or empty:
        private void richTxtBox_Emp_Enter(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.richTxtBox_Emp.Text))
            {
                SendKeys.Send("{Tab}");
            }
        }
        #endregion
        private void richTxtBox_Emp_LinkClicked(object sender, LinkClickedEventArgs e)//richTxtBox Link(Emp)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }


        #region open new windows
        //New Employee Window:
        private void stpNewEmp_Click(object sender, EventArgs e)
        {
            this.itsNewEmpOrEdit = "ImstpNewEmp";

            AddEmployee OpenTheWinAddEmp = new AddEmployee(this);
            OpenTheWinAddEmp.ShowDialog();
        }
        //New Skill Window:
        private void stpNewSkill_Click(object sender, EventArgs e)
        {
            this.itsNewEmpOrEdit = "ImstpNewSkl";

            AddSkill OpenTheWinAddSkill = new AddSkill(this);
            OpenTheWinAddSkill.ShowDialog();
        }
        #endregion


        //Exit:
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure do you want to exit?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.Close();
        }


        //Edit (By selected row):
        private void stpEdit_Click(object sender, EventArgs e)
        {
            this.itsNewEmpOrEdit = "ImstpEdit";

            #region Edit Emp
            if (this.tabControlGrds.SelectedIndex == 0) //All Emp
            {
                if (this.grdEmp.SelectedRows.Count > 0)
                {
                    AddEmployee OpenTheWinAddEmp = new AddEmployee(this);

                    int selectedRowInfo = int.Parse(this.grdEmp.SelectedRows[0].Cells["tblID"].Value.ToString());
                    int index = (this.lstEmps.FindIndex(x => x.ID == selectedRowInfo));

                    OpenTheWinAddEmp.txtFirstName.Text = this.lstEmps[index].FirstName;
                    OpenTheWinAddEmp.txtLastName.Text = this.lstEmps[index].LastName;
                    OpenTheWinAddEmp.txtID.Text = this.lstEmps[index].ID.ToString();
                    OpenTheWinAddEmp.txtSalary.Text = this.lstEmps[index].Salary.ToString();
                    OpenTheWinAddEmp.txtEmail.Text = this.lstEmps[index].Email;
                    OpenTheWinAddEmp.pictureBoxEmpAdd.ImageLocation = this.lstEmps[index].Image;
                    OpenTheWinAddEmp.timeAddStart.Value = this.lstEmps[index].StartToWorkAt;
                    OpenTheWinAddEmp.txtCV.Text = this.lstEmps[index].CV;

                    OpenTheWinAddEmp.firstEditID = int.Parse(OpenTheWinAddEmp.txtID.Text);

                    //the skill check-box initiolize into --> AddEmployee --> PrintSkillCheckBox()

                    #region set gender
                    if (this.lstEmps[index].GenderEmp == Gender.Female)
                    {
                        OpenTheWinAddEmp.radFemale.Select();
                    }
                    else
                    {
                        OpenTheWinAddEmp.radMale.Select();
                    }
                    #endregion

                    OpenTheWinAddEmp.ShowDialog();
                }
                else
                {
                    MessageBox.Show("There is no employee to edit!");
                }
            }
            #endregion

            #region Edit Skl
            else if (this.tabControlGrds.SelectedIndex == 1)//All Skill
            {
                if (this.grdSkl.SelectedRows.Count > 0)
                {
                    AddSkill OpenTheWinAddSkill = new AddSkill(this);

                    int selectedRowInfo = int.Parse(this.grdSkl.SelectedRows[0].Cells["tblBarcode"].Value.ToString());
                    int index = (this.lstSkills.FindIndex(x => x.Barcode == selectedRowInfo));

                    OpenTheWinAddSkill.txtBarcode.Text = this.lstSkills[index].Barcode.ToString();
                    OpenTheWinAddSkill.txtSkillName.Text = this.lstSkills[index].SkillName;
                    OpenTheWinAddSkill.txtDescription.Text = this.lstSkills[index].Description;
                    OpenTheWinAddSkill.txtAddLink.Text = this.lstSkills[index].LinkSkillInfo;
                    OpenTheWinAddSkill.firstEditBarcode = int.Parse(OpenTheWinAddSkill.txtBarcode.Text);

                    OpenTheWinAddSkill.ShowDialog();
                }
                else
                {
                    MessageBox.Show("There is no skill to edit!");
                }
            }
#endregion
        }


        //Selected Index Changed -tabControlEmpInfo- (Choose just: Emp-info or CV)
        private void tabControlEmpInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControlEmpInfo.SelectedIndex == 0)
            {
                this.richTxtBox_Emp.ScrollBars = RichTextBoxScrollBars.None;
                this.pictureBoxEmp.Visible = true;
                this.grdEmp_SelectionChanged(sender, e);

            }
            else if (this.tabControlEmpInfo.SelectedIndex == 1)
            {
                this.richTxtBox_Emp.ScrollBars = RichTextBoxScrollBars.Vertical;
                this.pictureBoxEmp.Visible = false;
                this.richTxtBox_Emp.Text = null;

                this.PrintCV();
            }
        }


        #region rad

        private void radFirstName_CheckedChanged(object sender, EventArgs e)
        {
            this.lblSkills.Enabled = false;
            this.cmbSkills.Enabled = false;
            this.lblDate.Enabled = false;
            this.timeSearchDate.Enabled = false;
            this.lblSearch.Enabled = true;
            this.txtSearch.Enabled = true;
            this.lblFrom.Enabled = false;
            this.lblTo.Enabled = false;
            this.txtSalaryFrom.Enabled = false;
            this.txtSalaryTo.Enabled = false;
            this.txtSalaryFrom.Text = null;
            this.txtSalaryTo.Text = null;
        }

        private void radLastName_CheckedChanged(object sender, EventArgs e)
        {
            this.lblSkills.Enabled = false;
            this.cmbSkills.Enabled = false;
            this.lblDate.Enabled = false;
            this.timeSearchDate.Enabled = false;
            this.lblSearch.Enabled = true;
            this.txtSearch.Enabled = true;
            this.lblFrom.Enabled = false;
            this.lblTo.Enabled = false;
            this.txtSalaryFrom.Enabled = false;
            this.txtSalaryTo.Enabled = false;
            this.txtSalaryFrom.Text = null;
            this.txtSalaryTo.Text = null;
        }

        private void radID_CheckedChanged(object sender, EventArgs e)
        {
            this.lblSkills.Enabled = false;
            this.cmbSkills.Enabled = false;
            this.lblDate.Enabled = false;
            this.timeSearchDate.Enabled = false;
            this.lblSearch.Enabled = true;
            this.txtSearch.Enabled = true;
            this.lblFrom.Enabled = false;
            this.lblTo.Enabled = false;
            this.txtSalaryFrom.Enabled = false;
            this.txtSalaryTo.Enabled = false;
            this.txtSalaryFrom.Text = null;
            this.txtSalaryTo.Text = null;
        }

        private void radEmail_CheckedChanged(object sender, EventArgs e)
        {
            this.lblSkills.Enabled = false;
            this.cmbSkills.Enabled = false;
            this.lblDate.Enabled = false;
            this.timeSearchDate.Enabled = false;
            this.lblSearch.Enabled = true;
            this.txtSearch.Enabled = true;
            this.lblFrom.Enabled = false;
            this.lblTo.Enabled = false;
            this.txtSalaryFrom.Enabled = false;
            this.txtSalaryTo.Enabled = false;
            this.txtSalaryFrom.Text = null;
            this.txtSalaryTo.Text = null;
        }

        private void radSkill_CheckedChanged(object sender, EventArgs e)
        {
            this.lblSearch.Enabled = false;
            this.txtSearch.Enabled = false;
            this.lblDate.Enabled = false;
            this.timeSearchDate.Enabled = false;
            this.lblSkills.Enabled = true;
            this.cmbSkills.Enabled = true;
            this.lblFrom.Enabled = false;
            this.lblTo.Enabled = false;
            this.txtSalaryFrom.Enabled = false;
            this.txtSalaryTo.Enabled = false;
            this.txtSearch.Text = null;
            this.txtSalaryFrom.Text = null;
            this.txtSalaryTo.Text = null;
            this.cmbSkills.Items.Clear();
            foreach (Skills skl in this.lstSkills)
            {
                this.cmbSkills.Items.Add(skl.SkillName);
            }
        }

        private void radDate_CheckedChanged(object sender, EventArgs e)
        {
            this.lblSkills.Enabled = false;
            this.cmbSkills.Enabled = false;
            this.lblSearch.Enabled = false;
            this.txtSearch.Enabled = false;
            this.lblDate.Enabled = true;
            this.timeSearchDate.Enabled = true;
            this.lblFrom.Enabled = false;
            this.lblTo.Enabled = false;
            this.txtSalaryFrom.Enabled = false;
            this.txtSalaryTo.Enabled = false;
            this.txtSearch.Text = null;
            this.txtSalaryFrom.Text = null;
            this.txtSalaryTo.Text = null;
        }

        private void radSalary_CheckedChanged(object sender, EventArgs e)
        {
            this.lblSkills.Enabled = false;
            this.cmbSkills.Enabled = false;
            this.lblSearch.Enabled = false;
            this.txtSearch.Enabled = false;
            this.lblDate.Enabled = false;
            this.timeSearchDate.Enabled = false;
            this.lblFrom.Enabled = true;
            this.lblTo.Enabled = true;
            this.txtSalaryFrom.Enabled = true;
            this.txtSalaryTo.Enabled = true;
            this.txtSearch.Text = null;
        }

        #endregion

        //Search Go!:
        private void btnSearchGo_Click(object sender, EventArgs e)
        {
            int doItOneTime = 1;

            #region by Name
            //Name
            if (this.radFirstName.Checked == true)
            {
                if (string.IsNullOrEmpty(this.txtSearch.Text))
                {
                    MessageBox.Show("Please provide the following \"Search\"");
                }
                else
                {
                    int howMuchContains = 0;
                    foreach (Employee emp in this.lstEmps)
                    {
                        bool contains = Regex.IsMatch(emp.FirstName, this.txtSearch.Text, RegexOptions.IgnoreCase);
                        if (contains == true)
                        {
                            howMuchContains++;
                            if (doItOneTime == 1)
                            {                          
                                this.grdEmp.Rows.Clear();
                                doItOneTime++;
                            }
                            this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                        }
                    }
                    if (howMuchContains == 0)
                    {
                        MessageBox.Show("No results found for: \"" + this.txtSearch.Text + "\"");
                    }
                }
            }
            #endregion

            #region by Last Name
            //Last Name
            else if (this.radLastName.Checked == true)
            {
                if (string.IsNullOrEmpty(this.txtSearch.Text))
                {
                    MessageBox.Show("Please provide the following \"Search\"");
                }
                else
                {
                    int howMuchContains = 0;
                    foreach (Employee emp in this.lstEmps)
                    {
                        bool contains = Regex.IsMatch(emp.LastName, this.txtSearch.Text, RegexOptions.IgnoreCase);
                        if (contains == true)
                        {
                            howMuchContains++;
                            if (doItOneTime == 1)
                            {
                                this.grdEmp.Rows.Clear();
                                doItOneTime++;
                            }
                            this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                        }
                    }
                    if (howMuchContains == 0)
                    {
                        MessageBox.Show("No results found for: \"" + this.txtSearch.Text + "\"");
                    }
                }
            }
            #endregion
                
            #region by ID
            //ID
            else if (this.radID.Checked == true)
            {
                if (string.IsNullOrEmpty(this.txtSearch.Text))
                {
                    MessageBox.Show("Please provide the following \"Search\"");
                }
                else
                {
                    int id;
                    int howMuchContains = 0;
                    if (int.TryParse(this.txtSearch.Text, out id) == true)
                    {
                        foreach (Employee emp in this.lstEmps)
                        {
                            bool contains = Regex.IsMatch(emp.ID.ToString(), this.txtSearch.Text, RegexOptions.IgnoreCase);
                            if (contains == true)
                            {
                                howMuchContains++;
                                if (doItOneTime == 1)
                                {
                                    this.grdEmp.Rows.Clear();
                                    doItOneTime++;
                                }
                                this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                            }
                        }
                        if (howMuchContains == 0)
                        {
                            MessageBox.Show("No results found for: \"" + this.txtSearch.Text + "\"");
                        }
                    }
                    else
                    {
                        MessageBox.Show("The ID must contain only numbers");
                    }
                }
            }
            #endregion

            #region by Email
            //Email
            else if (this.radEmail.Checked == true)
            {
                if (string.IsNullOrEmpty(this.txtSearch.Text))
                {
                    MessageBox.Show("Please provide the following \"Search\"");
                }
                else
                {
                    int howMuchContains = 0;
                    foreach (Employee emp in this.lstEmps)
                    {
                        bool contains = Regex.IsMatch(emp.Email, this.txtSearch.Text, RegexOptions.IgnoreCase);
                        if (contains == true)
                        {
                            howMuchContains++;
                            if (doItOneTime == 1)
                            {
                                this.grdEmp.Rows.Clear();
                                doItOneTime++;
                            }
                            this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                        }
                    }
                    if (howMuchContains == 0)
                    {
                        MessageBox.Show("No results found for: \"" + this.txtSearch.Text + "\"");
                    }
                }
            }
            #endregion

            #region by Salary
            //Salary
            else if (this.radSalary.Checked == true)
            {
                if ((string.IsNullOrEmpty(this.txtSalaryTo.Text)) && (string.IsNullOrEmpty(this.txtSalaryFrom.Text)))
                {
                    MessageBox.Show("Please provide one of the following \"From/To\"");
                }

                else if (string.IsNullOrEmpty(this.txtSalaryTo.Text))  //From(+)
                {
                    int salaryFrom;
                    if (int.TryParse(this.txtSalaryFrom.Text, out salaryFrom) == true)
                    {
                        int howMuchContains = 0;
                        var Emp = from c in this.lstEmps
                                  where c.Salary > salaryFrom
                                  select c;
                        foreach (Employee emp in Emp)
                        {
                            howMuchContains++;
                            if (doItOneTime == 1)
                            {
                                this.grdEmp.Rows.Clear();
                                doItOneTime++;
                            }
                            this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                        }
                        if (howMuchContains == 0)
                        {
                            MessageBox.Show("There is No salary higher than " + txtSalaryFrom.Text + "$");
                        }
                    }
                    else
                    {
                        MessageBox.Show("The Salary must contain only numbers");
                    }
                }

                else if (string.IsNullOrEmpty(this.txtSalaryFrom.Text)) //TO(-)
                {
                    int salaryTo;
                    if (int.TryParse(this.txtSalaryTo.Text, out salaryTo) == true)
                    {
                        int howMuchContains = 0;
                        var Emp = from c in this.lstEmps
                                  where c.Salary < salaryTo
                                  select c;
                        foreach (Employee emp in Emp)
                        {
                            howMuchContains++;
                            if (doItOneTime == 1)
                            {
                                this.grdEmp.Rows.Clear();
                                doItOneTime++;
                            }
                            this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                        }
                        if (howMuchContains == 0)
                        {
                            MessageBox.Show("There is No salary lower than " + txtSalaryTo.Text + "$");
                        }
                    }
                    else
                    {
                        MessageBox.Show("The Salary must contain only numbers");
                    }
                }
                else //From(+) && To(-)
                {
                    int salaryFrom;
                    int salaryTo;
                    if ((int.TryParse(this.txtSalaryFrom.Text, out salaryFrom) == true) && (int.TryParse(this.txtSalaryTo.Text, out salaryTo) == true))
                    {
                        int howMuchContains = 0;
                        var Emp = from c in this.lstEmps
                                  where c.Salary > salaryFrom && c.Salary < salaryTo
                                  select c;
                        foreach (Employee emp in Emp)
                        {
                            howMuchContains++;
                            if (doItOneTime == 1)
                            {
                                this.grdEmp.Rows.Clear();
                                doItOneTime++;
                            }
                            this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                        }
                        if (howMuchContains == 0)
                        {
                            MessageBox.Show("There is No salary higher than " + this.txtSalaryFrom.Text + "$ and lower than " + txtSalaryTo.Text + "$");
                        }
                    }
                    else
                    {
                        MessageBox.Show("The Salary must contain only numbers");
                    }
                }
            }
            #endregion

            #region by Skill
            else if (this.radSkill.Checked == true)
            {
                if (string.IsNullOrEmpty(this.cmbSkills.Text))
                {
                    MessageBox.Show("Please select a skill");
                }
                else
                {
                    foreach (Employee emp in this.lstEmps)
                    {
                        if (emp.Skill.Contains(this.cmbSkills.Text) == true)
                        {
                            if (doItOneTime == 1)
                            {
                                this.grdEmp.Rows.Clear();
                                doItOneTime++;
                            }
                            this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                        } 
                    }
                }
            }
#endregion

            #region by Date
            else if (this.radDate.Checked == true)
            {
                int howMuchContains = 0;
                var userTime = DateTime.Parse(this.timeSearchDate.Text);
                var Emp = from c in lstEmps
                          where c.StartToWorkAt == userTime 
                          select c;

                foreach (Employee emp in Emp)
                {
                    howMuchContains++;
                    if (doItOneTime == 1)
                    {
                        this.grdEmp.Rows.Clear();
                        doItOneTime++;
                    }
                    this.grdEmp.Rows.Add(emp.ID, emp.FirstName, emp.LastName, emp.Skill, emp.Salary, emp.Email, emp.PrintStartWork(), emp, emp.Image, emp.GenderEmp);
                }
                if (howMuchContains == 0)
                {
                    MessageBox.Show("No results found for: " + userTime.Day +"/"+ userTime.Month +"/"+ userTime.Year);
                }
            }
            #endregion
        }
        //search Clear:
        private void btnSearchClear_Click(object sender, EventArgs e)
        {
            this.grdEmp.Rows.Clear();
            this.PrintGrdEmp();
            this.cmbSkills.Text = null;
            this.txtSearch.Text = null;
            this.txtSalaryFrom.Text = null;
            this.txtSalaryTo.Text = null;
            this.timeSearchDate.ResetText();
        }
    }
}


