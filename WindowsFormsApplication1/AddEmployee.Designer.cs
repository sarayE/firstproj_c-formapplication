﻿namespace WindowsFormsApplication1
{
    partial class AddEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.btnSubmitEmp = new System.Windows.Forms.Button();
            this.lblStart = new System.Windows.Forms.Label();
            this.timeAddStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSalary = new System.Windows.Forms.Label();
            this.txtSalary = new System.Windows.Forms.TextBox();
            this.grpImage = new System.Windows.Forms.GroupBox();
            this.pictureBoxEmpAdd = new System.Windows.Forms.PictureBox();
            this.btnDelImage = new System.Windows.Forms.Button();
            this.btnAddImage = new System.Windows.Forms.Button();
            this.grpChooseSkills = new System.Windows.Forms.GroupBox();
            this.panel = new System.Windows.Forms.Panel();
            this.radMale = new System.Windows.Forms.RadioButton();
            this.radFemale = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelAll = new System.Windows.Forms.Panel();
            this.grpCV = new System.Windows.Forms.GroupBox();
            this.btnOpenFileCV = new System.Windows.Forms.Button();
            this.btnAttachCV = new System.Windows.Forms.Button();
            this.btnClearCV = new System.Windows.Forms.Button();
            this.txtCV = new System.Windows.Forms.TextBox();
            this.grpImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpAdd)).BeginInit();
            this.grpChooseSkills.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelAll.SuspendLayout();
            this.grpCV.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.ForeColor = System.Drawing.Color.Red;
            this.lblFirstName.Location = new System.Drawing.Point(10, 44);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(80, 17);
            this.lblFirstName.TabIndex = 12;
            this.lblFirstName.Text = "First Name:";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.ForeColor = System.Drawing.Color.Red;
            this.lblLastName.Location = new System.Drawing.Point(10, 75);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(80, 17);
            this.lblLastName.TabIndex = 13;
            this.lblLastName.Text = "Last Name:";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(95, 44);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(185, 22);
            this.txtFirstName.TabIndex = 14;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(95, 75);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(185, 22);
            this.txtLastName.TabIndex = 15;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.ForeColor = System.Drawing.Color.Red;
            this.lblID.Location = new System.Drawing.Point(10, 107);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(25, 17);
            this.lblID.TabIndex = 16;
            this.lblID.Text = "ID:";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(95, 107);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(185, 22);
            this.txtID.TabIndex = 17;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(10, 205);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(46, 17);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "Email:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(95, 205);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(185, 22);
            this.txtEmail.TabIndex = 19;
            // 
            // btnSubmitEmp
            // 
            this.btnSubmitEmp.Location = new System.Drawing.Point(5, 369);
            this.btnSubmitEmp.Name = "btnSubmitEmp";
            this.btnSubmitEmp.Size = new System.Drawing.Size(267, 60);
            this.btnSubmitEmp.TabIndex = 12;
            this.btnSubmitEmp.Text = "Submit Employee";
            this.btnSubmitEmp.UseVisualStyleBackColor = true;
            this.btnSubmitEmp.Click += new System.EventHandler(this.btnSubmitEmp_Click);
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(10, 238);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(42, 17);
            this.lblStart.TabIndex = 20;
            this.lblStart.Text = "Start:";
            // 
            // timeAddStart
            // 
            this.timeAddStart.Location = new System.Drawing.Point(95, 238);
            this.timeAddStart.Name = "timeAddStart";
            this.timeAddStart.Size = new System.Drawing.Size(185, 22);
            this.timeAddStart.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(10, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "Gender:";
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Location = new System.Drawing.Point(10, 172);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(52, 17);
            this.lblSalary.TabIndex = 27;
            this.lblSalary.Text = "Salary:";
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(94, 172);
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(185, 22);
            this.txtSalary.TabIndex = 28;
            // 
            // grpImage
            // 
            this.grpImage.Controls.Add(this.pictureBoxEmpAdd);
            this.grpImage.Controls.Add(this.btnDelImage);
            this.grpImage.Controls.Add(this.btnAddImage);
            this.grpImage.Location = new System.Drawing.Point(286, 14);
            this.grpImage.Name = "grpImage";
            this.grpImage.Size = new System.Drawing.Size(176, 227);
            this.grpImage.TabIndex = 32;
            this.grpImage.TabStop = false;
            this.grpImage.Text = "Image:";
            // 
            // pictureBoxEmpAdd
            // 
            this.pictureBoxEmpAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxEmpAdd.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBoxEmpAdd.ErrorImage = null;
            this.pictureBoxEmpAdd.Location = new System.Drawing.Point(81, 11);
            this.pictureBoxEmpAdd.Name = "pictureBoxEmpAdd";
            this.pictureBoxEmpAdd.Size = new System.Drawing.Size(84, 126);
            this.pictureBoxEmpAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEmpAdd.TabIndex = 30;
            this.pictureBoxEmpAdd.TabStop = false;
            // 
            // btnDelImage
            // 
            this.btnDelImage.Location = new System.Drawing.Point(81, 182);
            this.btnDelImage.Name = "btnDelImage";
            this.btnDelImage.Size = new System.Drawing.Size(84, 31);
            this.btnDelImage.TabIndex = 31;
            this.btnDelImage.Text = "Delet";
            this.btnDelImage.UseVisualStyleBackColor = true;
            this.btnDelImage.Click += new System.EventHandler(this.btnDelImage_Click);
            // 
            // btnAddImage
            // 
            this.btnAddImage.Location = new System.Drawing.Point(81, 145);
            this.btnAddImage.Name = "btnAddImage";
            this.btnAddImage.Size = new System.Drawing.Size(84, 31);
            this.btnAddImage.TabIndex = 29;
            this.btnAddImage.Text = "Add";
            this.btnAddImage.UseVisualStyleBackColor = true;
            this.btnAddImage.Click += new System.EventHandler(this.btnAddImage_Click);
            // 
            // grpChooseSkills
            // 
            this.grpChooseSkills.Controls.Add(this.panel);
            this.grpChooseSkills.Location = new System.Drawing.Point(286, 252);
            this.grpChooseSkills.Name = "grpChooseSkills";
            this.grpChooseSkills.Size = new System.Drawing.Size(176, 177);
            this.grpChooseSkills.TabIndex = 33;
            this.grpChooseSkills.TabStop = false;
            this.grpChooseSkills.Text = "Choose Skills:";
            // 
            // panel
            // 
            this.panel.AutoScroll = true;
            this.panel.Location = new System.Drawing.Point(6, 21);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(159, 150);
            this.panel.TabIndex = 0;
            // 
            // radMale
            // 
            this.radMale.AutoSize = true;
            this.radMale.Location = new System.Drawing.Point(19, 11);
            this.radMale.Name = "radMale";
            this.radMale.Size = new System.Drawing.Size(59, 21);
            this.radMale.TabIndex = 34;
            this.radMale.TabStop = true;
            this.radMale.Text = "Male";
            this.radMale.UseVisualStyleBackColor = true;
            this.radMale.CheckedChanged += new System.EventHandler(this.radMale_CheckedChanged);
            // 
            // radFemale
            // 
            this.radFemale.AutoSize = true;
            this.radFemale.Location = new System.Drawing.Point(95, 11);
            this.radFemale.Name = "radFemale";
            this.radFemale.Size = new System.Drawing.Size(75, 21);
            this.radFemale.TabIndex = 35;
            this.radFemale.TabStop = true;
            this.radFemale.Text = "Female";
            this.radFemale.UseVisualStyleBackColor = true;
            this.radFemale.CheckedChanged += new System.EventHandler(this.radFemale_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radFemale);
            this.groupBox1.Controls.Add(this.radMale);
            this.groupBox1.Location = new System.Drawing.Point(95, 130);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(185, 36);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            // 
            // panelAll
            // 
            this.panelAll.AutoScroll = true;
            this.panelAll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAll.Controls.Add(this.grpCV);
            this.panelAll.Controls.Add(this.grpImage);
            this.panelAll.Controls.Add(this.grpChooseSkills);
            this.panelAll.Controls.Add(this.btnSubmitEmp);
            this.panelAll.Location = new System.Drawing.Point(7, 18);
            this.panelAll.Name = "panelAll";
            this.panelAll.Size = new System.Drawing.Size(474, 460);
            this.panelAll.TabIndex = 1;
            // 
            // grpCV
            // 
            this.grpCV.Controls.Add(this.btnOpenFileCV);
            this.grpCV.Controls.Add(this.btnAttachCV);
            this.grpCV.Controls.Add(this.btnClearCV);
            this.grpCV.Controls.Add(this.txtCV);
            this.grpCV.Location = new System.Drawing.Point(5, 252);
            this.grpCV.Name = "grpCV";
            this.grpCV.Size = new System.Drawing.Size(267, 97);
            this.grpCV.TabIndex = 33;
            this.grpCV.TabStop = false;
            this.grpCV.Text = "CV:";
            // 
            // btnOpenFileCV
            // 
            this.btnOpenFileCV.Location = new System.Drawing.Point(181, 55);
            this.btnOpenFileCV.Name = "btnOpenFileCV";
            this.btnOpenFileCV.Size = new System.Drawing.Size(79, 31);
            this.btnOpenFileCV.TabIndex = 38;
            this.btnOpenFileCV.Text = "Open File";
            this.btnOpenFileCV.UseVisualStyleBackColor = true;
            this.btnOpenFileCV.Click += new System.EventHandler(this.btnOpenFileCV_Click);
            // 
            // btnAttachCV
            // 
            this.btnAttachCV.Image = global::WindowsFormsApplication1.Properties.Resources.attach_icon;
            this.btnAttachCV.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAttachCV.Location = new System.Drawing.Point(6, 55);
            this.btnAttachCV.Name = "btnAttachCV";
            this.btnAttachCV.Size = new System.Drawing.Size(79, 31);
            this.btnAttachCV.TabIndex = 29;
            this.btnAttachCV.Text = "  Attach";
            this.btnAttachCV.UseVisualStyleBackColor = true;
            this.btnAttachCV.Click += new System.EventHandler(this.btnAttachCV_Click);
            // 
            // btnClearCV
            // 
            this.btnClearCV.Location = new System.Drawing.Point(94, 55);
            this.btnClearCV.Name = "btnClearCV";
            this.btnClearCV.Size = new System.Drawing.Size(79, 31);
            this.btnClearCV.TabIndex = 31;
            this.btnClearCV.Text = "Clear";
            this.btnClearCV.UseVisualStyleBackColor = true;
            this.btnClearCV.Click += new System.EventHandler(this.btnClearCV_Click);
            // 
            // txtCV
            // 
            this.txtCV.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtCV.Location = new System.Drawing.Point(6, 23);
            this.txtCV.Name = "txtCV";
            this.txtCV.ReadOnly = true;
            this.txtCV.Size = new System.Drawing.Size(254, 22);
            this.txtCV.TabIndex = 37;
            // 
            // AddEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 494);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtSalary);
            this.Controls.Add(this.lblSalary);
            this.Controls.Add(this.lblFirstName);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.timeAddStart);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.lblStart);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.panelAll);
            this.MaximizeBox = false;
            this.Name = "AddEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Employee";
            this.grpImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpAdd)).EndInit();
            this.grpChooseSkills.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelAll.ResumeLayout(false);
            this.grpCV.ResumeLayout(false);
            this.grpCV.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnSubmitEmp;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSalary;
        private System.Windows.Forms.GroupBox grpImage;
        private System.Windows.Forms.Button btnDelImage;
        private System.Windows.Forms.Button btnAddImage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panelAll;
        public System.Windows.Forms.TextBox txtFirstName;
        public System.Windows.Forms.TextBox txtLastName;
        public System.Windows.Forms.TextBox txtID;
        public System.Windows.Forms.TextBox txtEmail;
        public System.Windows.Forms.TextBox txtSalary;
        public System.Windows.Forms.PictureBox pictureBoxEmpAdd;
        public System.Windows.Forms.RadioButton radMale;
        public System.Windows.Forms.RadioButton radFemale;
        public System.Windows.Forms.DateTimePicker timeAddStart;
        public System.Windows.Forms.Panel panel;
        public System.Windows.Forms.GroupBox grpChooseSkills;
        private System.Windows.Forms.GroupBox grpCV;
        public System.Windows.Forms.TextBox txtCV;
        private System.Windows.Forms.Button btnClearCV;
        private System.Windows.Forms.Button btnAttachCV;
        private System.Windows.Forms.Button btnOpenFileCV;
    }
}