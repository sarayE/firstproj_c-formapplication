﻿namespace WindowsFormsApplication1
{
    partial class AddSkill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelAll = new System.Windows.Forms.Panel();
            this.txtAddLink = new System.Windows.Forms.TextBox();
            this.lblAddLink = new System.Windows.Forms.Label();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.lblSkillName = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.txtSkillName = new System.Windows.Forms.TextBox();
            this.btnSubmitSkill = new System.Windows.Forms.Button();
            this.lblDescribe = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.panelAll.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelAll
            // 
            this.panelAll.AutoScroll = true;
            this.panelAll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAll.Controls.Add(this.txtAddLink);
            this.panelAll.Controls.Add(this.lblAddLink);
            this.panelAll.Controls.Add(this.lblBarcode);
            this.panelAll.Controls.Add(this.lblSkillName);
            this.panelAll.Controls.Add(this.txtBarcode);
            this.panelAll.Controls.Add(this.txtSkillName);
            this.panelAll.Controls.Add(this.btnSubmitSkill);
            this.panelAll.Controls.Add(this.lblDescribe);
            this.panelAll.Controls.Add(this.txtDescription);
            this.panelAll.Location = new System.Drawing.Point(8, 12);
            this.panelAll.Name = "panelAll";
            this.panelAll.Size = new System.Drawing.Size(286, 302);
            this.panelAll.TabIndex = 2;
            // 
            // txtAddLink
            // 
            this.txtAddLink.Location = new System.Drawing.Point(87, 195);
            this.txtAddLink.Name = "txtAddLink";
            this.txtAddLink.Size = new System.Drawing.Size(185, 22);
            this.txtAddLink.TabIndex = 35;
            // 
            // lblAddLink
            // 
            this.lblAddLink.AutoSize = true;
            this.lblAddLink.ForeColor = System.Drawing.Color.Black;
            this.lblAddLink.Location = new System.Drawing.Point(9, 196);
            this.lblAddLink.Name = "lblAddLink";
            this.lblAddLink.Size = new System.Drawing.Size(67, 17);
            this.lblAddLink.TabIndex = 34;
            this.lblAddLink.Text = "Add Link:";
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.ForeColor = System.Drawing.Color.Red;
            this.lblBarcode.Location = new System.Drawing.Point(2, 21);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(65, 17);
            this.lblBarcode.TabIndex = 18;
            this.lblBarcode.Text = "Barcode:";
            // 
            // lblSkillName
            // 
            this.lblSkillName.AutoSize = true;
            this.lblSkillName.ForeColor = System.Drawing.Color.Red;
            this.lblSkillName.Location = new System.Drawing.Point(2, 52);
            this.lblSkillName.Name = "lblSkillName";
            this.lblSkillName.Size = new System.Drawing.Size(80, 17);
            this.lblSkillName.TabIndex = 19;
            this.lblSkillName.Text = "SKill Name:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(87, 21);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(185, 22);
            this.txtBarcode.TabIndex = 20;
            // 
            // txtSkillName
            // 
            this.txtSkillName.Location = new System.Drawing.Point(87, 52);
            this.txtSkillName.Name = "txtSkillName";
            this.txtSkillName.Size = new System.Drawing.Size(185, 22);
            this.txtSkillName.TabIndex = 21;
            // 
            // btnSubmitSkill
            // 
            this.btnSubmitSkill.Location = new System.Drawing.Point(12, 237);
            this.btnSubmitSkill.Name = "btnSubmitSkill";
            this.btnSubmitSkill.Size = new System.Drawing.Size(260, 44);
            this.btnSubmitSkill.TabIndex = 12;
            this.btnSubmitSkill.Text = "Submit Skill";
            this.btnSubmitSkill.UseVisualStyleBackColor = true;
            this.btnSubmitSkill.Click += new System.EventHandler(this.btnSubmitSkill_Click);
            // 
            // lblDescribe
            // 
            this.lblDescribe.AutoSize = true;
            this.lblDescribe.ForeColor = System.Drawing.Color.Red;
            this.lblDescribe.Location = new System.Drawing.Point(2, 84);
            this.lblDescribe.Name = "lblDescribe";
            this.lblDescribe.Size = new System.Drawing.Size(68, 17);
            this.lblDescribe.TabIndex = 22;
            this.lblDescribe.Text = "Describe:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(87, 84);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(185, 98);
            this.txtDescription.TabIndex = 23;
            // 
            // AddSkill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 327);
            this.Controls.Add(this.panelAll);
            this.MaximizeBox = false;
            this.Name = "AddSkill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddSkill";
            this.panelAll.ResumeLayout(false);
            this.panelAll.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelAll;
        private System.Windows.Forms.Button btnSubmitSkill;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.Label lblSkillName;
        private System.Windows.Forms.Label lblDescribe;
        private System.Windows.Forms.Label lblAddLink;
        public System.Windows.Forms.TextBox txtBarcode;
        public System.Windows.Forms.TextBox txtSkillName;
        public System.Windows.Forms.TextBox txtDescription;
        public System.Windows.Forms.TextBox txtAddLink;

    }
}