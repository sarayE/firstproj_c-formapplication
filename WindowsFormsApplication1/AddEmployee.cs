﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class AddEmployee : Form
    {
        FormHR originalForm;
        public CheckBox chk;
        public int firstEditID;
        Gender SelctedGender { get; set; }
        public string CheckedSkill { get; set; }

        public AddEmployee(FormHR incomingForm)
        {
            originalForm = incomingForm;
            InitializeComponent();

            PrintSkillCheckBox();
        }

        //Submit:
        public void btnSubmitEmp_Click(object sender, EventArgs e)
        {
            #region Add New Emp - Submit
            if (originalForm.itsNewEmpOrEdit == "ImstpNewEmp")
            {
                if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && (!string.IsNullOrEmpty(this.txtID.Text)) && ((this.radMale.Checked == true) || (this.radFemale.Checked == true)))
                {
                    string firstName = this.txtFirstName.Text;
                    string lastName = this.txtLastName.Text;
                    int salary = 0;
                    string email = this.txtEmail.Text;
                    string cv = this.txtCV.Text;
                    DateTime startWorking = timeAddStart.Value;
                    CheckedSkill = "";
                    string skills = AddSkillToCheckedSkillProp();
                    int id;
                    if (int.TryParse(this.txtID.Text, out id) == true)
                    {
                        if (originalForm.lstEmps.Exists(x => x.ID == id))
                        {
                            MessageBox.Show("The Addition Failed!\n(There is already an employee with this ID)");
                        }

                        else
                        {
                            #region Are you sure do you want to Edit?
                            if (IsValidEmail(email) || string.IsNullOrEmpty(email))
                            {
                                if ((int.TryParse(this.txtSalary.Text, out salary) == true) || string.IsNullOrEmpty(this.txtSalary.Text))
                                {
                                    if (MessageBox.Show(string.Format("Are you sure do you want to Add \"{0} {1}\" ?", firstName, lastName), "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                    {
                                        this.CheckedIfMaleOrFemale();
                                        originalForm.lstEmps.Add(new Employee(firstName, lastName, id, SelctedGender)
                                        {
                                            Skill = skills,
                                            Salary = salary,
                                            Email = email,
                                            Image = pictureBoxEmpAdd.ImageLocation,
                                            StartToWorkAt = startWorking,
                                            CV = cv,
                                        });

                                        originalForm.grdEmp.Rows.Clear();
                                        originalForm.PrintGrdEmp();
                                        MessageBox.Show("Succeeded!");
                                        this.Close();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("The Salary must contain only numbers");
                                }
                            }
                            else
                            {
                                MessageBox.Show("The email address is invalid\n(Format: name@xxx.xx)");
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        MessageBox.Show("The ID must contain only numbers");
                    }
                }
                else
                {
                    this.ChekedIfIsNullOrEmpty();
                }
            }
            #endregion

            #region Edit Emp - Submit
            else if (originalForm.itsNewEmpOrEdit == "ImstpEdit") //EDIT-Submit!!!
            {
                if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && (!string.IsNullOrEmpty(this.txtID.Text)) && ((this.radMale.Checked == true) || (this.radFemale.Checked == true)))
                {
                    string firstName = this.txtFirstName.Text;
                    string lastName = this.txtLastName.Text;
                    int salary = 0;
                    string email = this.txtEmail.Text;
                    string cv = this.txtCV.Text;
                    DateTime startWorking = timeAddStart.Value;
                    CheckedSkill = "";
                    string skills = AddSkillToCheckedSkillProp();

                    int id;
                    if (int.TryParse(this.txtID.Text, out id) == true)
                    {
                        if (id != firstEditID)
                        {
                            if (originalForm.lstEmps.Exists(x => x.ID == id))
                            {
                                MessageBox.Show("The Addition Failed!\n(There is already an employee with this ID)");
                            }
                            else
                            {
                                #region Are you sure do you want to Edit?
                                if (IsValidEmail(email) || string.IsNullOrEmpty(email))
                                {
                                    if ((int.TryParse(this.txtSalary.Text, out salary) == true) || string.IsNullOrEmpty(this.txtSalary.Text))
                                    {
                                        if (MessageBox.Show(string.Format("Are you sure do you want to Edit \"{0} {1}\" ?", firstName, lastName), "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                        {
                                            CheckedIfMaleOrFemale();
                                            originalForm.lstEmps.Add(new Employee(firstName, lastName, id, SelctedGender)/////add new one
                                            {
                                                Skill = skills,
                                                Salary = salary,
                                                Email = email,
                                                Image = pictureBoxEmpAdd.ImageLocation,
                                                StartToWorkAt = startWorking,
                                                CV = cv,
                                            });

                                            int index = (originalForm.lstEmps.FindIndex(x => x.ID == firstEditID));
                                            originalForm.lstEmps.RemoveAt(index);///////remove old one
                                            originalForm.grdEmp.Rows.Clear();
                                            originalForm.PrintGrdEmp();
                                            MessageBox.Show("Succeeded!");
                                            this.Close();
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("The Salary must contain only numbers");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("The email address is invalid\n(Format: name@xxx.xx)");
                                }
                                #endregion
                            }
                        }

                        else
                        {
                            #region Are you sure do you want to Edit?
                            if (IsValidEmail(email) || string.IsNullOrEmpty(email))
                            {
                                if ((int.TryParse(this.txtSalary.Text, out salary) == true) || string.IsNullOrEmpty(this.txtSalary.Text))
                                {
                                    if (MessageBox.Show(string.Format("Are you sure do you want to Edit \"{0} {1}\" ?", firstName, lastName), "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                    {
                                        CheckedIfMaleOrFemale();
                                        originalForm.lstEmps.Add(new Employee(firstName, lastName, id, SelctedGender)/////add new one
                                        {
                                            Skill = skills,
                                            Salary = salary,
                                            Email = email,
                                            Image = pictureBoxEmpAdd.ImageLocation,
                                            StartToWorkAt = startWorking,
                                            CV = cv,
                                        });

                                        int index = (originalForm.lstEmps.FindIndex(x => x.ID == firstEditID));
                                        originalForm.lstEmps.RemoveAt(index);///////remove old one
                                        originalForm.grdEmp.Rows.Clear();
                                        originalForm.PrintGrdEmp();
                                        MessageBox.Show("Succeeded!");
                                        this.Close();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("The Salary must contain only numbers");
                                }
                            }
                            else
                            {
                                MessageBox.Show("The email address is invalid\n(Format: name@xxx.xx)");
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        MessageBox.Show("The ID must contain only numbers");
                    }
                }
                else
                {
                    this.ChekedIfIsNullOrEmpty();
                }
            }
            #endregion
        }


        //Email:
        private bool IsValidEmail(string email)
        {
            try
            {
                MailAddress mail = new MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }


        private void CheckedIfMaleOrFemale()
        {
            if (this.radMale.Checked == true)
            {
                SelctedGender = Gender.Male;
            }
            else if (this.radFemale.Checked == true)
            {
                SelctedGender = Gender.Female;
            }
        }
        // Male/Female defult image = (rad)
        private void radMale_CheckedChanged(object sender, EventArgs e)
        {
            if ((this.pictureBoxEmpAdd.ImageLocation == null) || (this.pictureBoxEmpAdd.ImageLocation == @"http://www.danubeconsul.eu/female.jpg"))
            {
                //this.pictureBoxEmpAdd.ImageLocation = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\MaleNoProfile.gif";
                this.pictureBoxEmpAdd.ImageLocation = @"http://philosophy.ucr.edu/wp-content/uploads/2014/10/no-profile-img.gif";

            }
        }
        private void radFemale_CheckedChanged(object sender, EventArgs e)
        {
            if ((this.pictureBoxEmpAdd.ImageLocation == null) || (this.pictureBoxEmpAdd.ImageLocation == @"http://philosophy.ucr.edu/wp-content/uploads/2014/10/no-profile-img.gif"))
            {
                //this.pictureBoxEmpAdd.ImageLocation = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\FemaleNoProfile.jpg";
                this.pictureBoxEmpAdd.ImageLocation = @"http://www.danubeconsul.eu/female.jpg";

            }
        }


        //Add/Del image:
        private void btnAddImage_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.pictureBoxEmpAdd.ImageLocation = openFileDialog.FileName;
            }
        }
        private void btnDelImage_Click(object sender, EventArgs e)
        {
            if (this.radMale.Checked == true)
            {
                //this.pictureBoxEmpAdd.ImageLocation = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\MaleNoProfile.gif";
                this.pictureBoxEmpAdd.ImageLocation = @"http://philosophy.ucr.edu/wp-content/uploads/2014/10/no-profile-img.gif";
            }
            else if (this.radFemale.Checked == true)
            {
                //this.pictureBoxEmpAdd.ImageLocation = @"C:\Users\Admin\Desktop\My Proj\WindowsFormsApplication1\EmployeesPictures\FemaleNoProfile.jpg";
                this.pictureBoxEmpAdd.ImageLocation = @"http://www.danubeconsul.eu/female.jpg";
            }
            else
            {
                this.pictureBoxEmpAdd.ImageLocation = null;
            }
            this.openFileDialog.FileName = null;
        }


        //Add - chk:
        public void PrintSkillCheckBox()
        {
            for (int i = 0; i < originalForm.lstSkills.Count; i++)
            {
                chk = new CheckBox();

                this.chk.Text = originalForm.grdSkl.Rows[i].Cells["tblSkillsName"].Value.ToString();
                this.chk.Name = "chk" + this.chk.Text + i;

                this.chk.AutoSize = true;
                this.chk.Location = new Point(5, 25 * panel.Controls.Count);
                this.panel.Controls.Add(chk);
                this.chk.CheckedChanged += chk_CheckedChanged;

                #region Edit Skill Checked:
                if ((originalForm.itsNewEmpOrEdit == "ImstpEdit") && (originalForm.grdEmp.SelectedRows.Count > 0) && (originalForm.tabControlGrds.SelectedIndex == 0))
                {
                    int selectedRowInfo = int.Parse(originalForm.grdEmp.SelectedRows[0].Cells["tblID"].Value.ToString());
                    int id = (originalForm.lstEmps.FindIndex(x => x.ID == selectedRowInfo));

                    string skillsname = originalForm.lstEmps[id].Skill;

                    if (skillsname.Contains(this.chk.Text) == true)
                    {
                        this.chk.Checked = true;
                    }
                }
                #endregion
            }
        }
        public List<string> lstCheckedSkills = new List<string>();
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            if (this.chk.Checked)
            {
                this.lstCheckedSkills.Add(chk.Text);
            }
            else
            {
                this.lstCheckedSkills.Remove(chk.Text);
            }
        }
        private string AddSkillToCheckedSkillProp()
        {
            for (int i = 0; i < this.lstCheckedSkills.Count; i++)
            {
                CheckedSkill += "\n" + this.lstCheckedSkills[i];
            }

            return CheckedSkill;
        }


        //Exception if the addition text-> isNullOrEmpty:
        private void ChekedIfIsNullOrEmpty()
        {
            if ((this.radMale.Checked == false) && (this.radFemale.Checked == false))
            {
                if (string.IsNullOrEmpty(this.txtFirstName.Text) && string.IsNullOrEmpty(this.txtLastName.Text) && string.IsNullOrEmpty(this.txtID.Text))
                {
                    MessageBox.Show("You must enter a First Name, Last Name, ID and a Gender");
                }
                else if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && (!string.IsNullOrEmpty(this.txtID.Text)))
                {
                    MessageBox.Show("You must enter a Gender");
                }
                else if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && string.IsNullOrEmpty(this.txtLastName.Text) && string.IsNullOrEmpty(this.txtID.Text))
                {
                    MessageBox.Show("You must enter a Last Name, ID and a Gender");
                }
                else if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && string.IsNullOrEmpty(this.txtID.Text))
                {
                    MessageBox.Show("You must enter an ID and a Gender");
                }
                else if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && string.IsNullOrEmpty(this.txtLastName.Text) && (!string.IsNullOrEmpty(this.txtID.Text)))
                {
                    MessageBox.Show("You must enter a Last Name and a Gender");
                }
                else if (string.IsNullOrEmpty(this.txtFirstName.Text) && string.IsNullOrEmpty(this.txtLastName.Text) && (!string.IsNullOrEmpty(this.txtID.Text)))
                {
                    MessageBox.Show("You must enter a First Name, Last Name and a Gender");
                }
                else if (string.IsNullOrEmpty(this.txtFirstName.Text) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && (!string.IsNullOrEmpty(this.txtID.Text)))
                {
                    MessageBox.Show("You must enter a First Name and a Gender");
                }
                else if (string.IsNullOrEmpty(this.txtFirstName.Text) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && string.IsNullOrEmpty(this.txtID.Text))
                {
                    MessageBox.Show("You must enter a First Name, ID and a Gender");
                }
            }
            else if (string.IsNullOrEmpty(this.txtFirstName.Text) && string.IsNullOrEmpty(this.txtLastName.Text) && string.IsNullOrEmpty(this.txtID.Text))
            {
                MessageBox.Show("You must enter a First Name, Last Name and an ID number");
            }
            else if (string.IsNullOrEmpty(this.txtFirstName.Text) && string.IsNullOrEmpty(this.txtLastName.Text) && string.IsNullOrEmpty(this.txtID.Text))
            {
                MessageBox.Show("You must enter a First Name, Last Name and an ID");
            }
            else if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && string.IsNullOrEmpty(this.txtLastName.Text) && string.IsNullOrEmpty(this.txtID.Text))
            {
                MessageBox.Show("You must enter a Last Name and an ID");
            }
            else if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && string.IsNullOrEmpty(this.txtID.Text))
            {
                MessageBox.Show("You must enter an ID");
            }
            else if ((!string.IsNullOrEmpty(this.txtFirstName.Text)) && string.IsNullOrEmpty(this.txtLastName.Text) && (!string.IsNullOrEmpty(this.txtID.Text)))
            {
                MessageBox.Show("You must enter a Last Name");
            }
            else if (string.IsNullOrEmpty(this.txtFirstName.Text) && string.IsNullOrEmpty(this.txtLastName.Text) && (!string.IsNullOrEmpty(this.txtID.Text)))
            {
                MessageBox.Show("You must enter a First Name and Last Name");
            }
            else if (string.IsNullOrEmpty(this.txtFirstName.Text) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && (!string.IsNullOrEmpty(this.txtID.Text)))
            {
                MessageBox.Show("You must enter a First Name");
            }
            else if (string.IsNullOrEmpty(this.txtFirstName.Text) && (!string.IsNullOrEmpty(this.txtLastName.Text)) && string.IsNullOrEmpty(this.txtID.Text))
            {
                MessageBox.Show("You must enter a First Name and an ID");
            }
        }


        //CV:
        private void btnAttachCV_Click(object sender, EventArgs e)
        {
            OpenFileDialog AddCV = new OpenFileDialog();
            AddCV.Filter = "Text Files |*.txt";

            if (AddCV.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.txtCV.Text = AddCV.FileName;
            }
        }
        private void btnClearCV_Click(object sender, EventArgs e)
        {
            this.txtCV.Text = null;
        }
        private void btnOpenFileCV_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtCV.Text))
            {
                MessageBox.Show("There is no file select");
            }
            else
            {
                System.Diagnostics.Process.Start("notepad.exe", txtCV.Text);
            }
        }

    }
}
