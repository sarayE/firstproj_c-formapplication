DVIR MELER
FINANCE MANAGER
Dvir@zahav.com

PROFESSIONAL :

Dayjob Ltd, 120 Vyse Street, Birmingham B18 6NF

Summary A highly motivated and results driven finance manager who has over three years of invaluable
experience in leading and developing a successful finance team. Skilled in numerous financial and
accounting fields, including: preparing annual budgets, monitoring key accounts and credit
control. Having the ability to handle complex assignments effectively & possessing the confidence
to work as part of a team or independently. Martin is presently looking for a suitable opportunity
position with a forward thinking company where he can excel, deliver & achieve his potential.

Skills :

Career FINANCE MANAGER May 2007 - Present
Accounting Company
Working a busy and high volume environment driving consistency and best practice across all the
businesses. Responsible for improving the companies cash flow and reducing its arrears by
keeping accurate records and ensuring payments are received on time.

* In charge of managing and supporting the ledger team.
* Providing accurate financial information to colleagues and senior managers
* Identifying areas for cost cutting and improvement.
* Ensuring that all financial controls for the division are met and adhered to at all times.
* Giving advice, guidance and support on all financial matter to the company directors.