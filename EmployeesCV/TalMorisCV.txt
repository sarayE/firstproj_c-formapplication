TAL MORIS
ABC UNIVERSITY -- Sometown, NY
MS in Software Engineering Candidate, 2010 to Present
Current GPA: 3.8 | Expected Graduation: 2012
TalMo@gmail.com

DEF UNIVERSITY -- Sometown, NJ
BS in Computer Information Science, 1998

Certifications:

    * Sun Certified Java Programmer (SCJP), 2007 
    * Sun Certified Java Developer (SCJD), 2008

Recent Seminars:

    * Enterprise Java Development
    * C++ Programming: Concepts, Design and Implementation
    * Windows Programming Using Visual C++
    * Advanced Workshop in Object-Oriented Analysis and Design
    * Project Lifecycle and Project Management Master Class
    * Project Management Methods

Volunteerism 	Little League Coach (Sometown, NY), 2009 to Present
PTA Secretary (Sometown Elementary School), 2010 to Present
Active Volunteer, Special Olympics, 2008 to Present